import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:memeorize/ForbiddenWords.dart';
import 'package:memeorize/friends.dart';
import 'package:memeorize/utils/model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'admin_password_field.dart';
import 'player_level.dart';
import 'ranking_bar.dart';
import 'database.dart';
import 'addmeme.dart';
import 'memeollection.dart';
import 'firebase_options.dart';
import 'package:provider/provider.dart';
import 'homepage_Firebase.dart';
import 'style.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

/*     --------------------------------//\\--------------------------------

    Marcel's Bereich

    Hautpseite, init und Navigationsbereiche

    Details:
      Die NavigationRail ist das Hauptnavigationstool, wird jedoch
      bei einem kleinerem Bildschrim zu einer BottomnagivationBar ersetzt

      Grundlegend habe ich versucht auf Responsiveness zu achten, musste jedoch
      gegen Ende leider alles "scrollable" machen, bzw. in einen 
      SingleChildScrollView packen, damit es auch wirklich 0 Fehler sind


      Für das Team:
        Kurzanleitung:
        -als ich das hier Schrieb hörte ich: 
          Finch - Kurzurlaub (bitte anhören jetzt)
        -Arbeiten könnt ihr ab Zeile 50 in die, 
        mit Kommentar benannten, Container

--------------------------------// Basics //--------------------------------
Basics:
  -Farben: Farben sind festgelegt, durch ein ColorSheme
  [Mobile Computing Folie 4.6 BuildContext]
  Beispiel: color: theme.of(context).colorSheme.secondary

  Hier eine Liste:
    seedColor: Color.fromARGB(255, 7, 7, 7), //Background Color / Menu links, Schwarz
    primary: Color.fromARGB(255, 14, 18, 20), //Background, Grau
    onPrimary: Color.fromARGB(255, 30, 32, 32), //Highlighted Element, leichtes Grau
    secondary: Color.fromARGB(255, 255, 255, 255), //Textfarbe, weiß
    onSecondary: Color.fromARGB(255, 221, 255, 0), //Highlited Text / Hauptfarbe, Grün

*/ //     --------------------------------//\\--------------------------------

Box<QuizData>? database;
void main() async {
  WidgetsFlutterBinding.ensureInitialized(); // Dies sollte zuerst kommen
  await initHive();
  database = await Hive.openBox<QuizData>('quizDataBox');
  PlayerLevel.prefs = await SharedPreferences.getInstance();
  await Supabase.initialize(
    url: 'https://dwwcudekvowwshiieidi.supabase.co',
    anonKey:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImR3d2N1ZGVrdm93d3NoaWllaWRpIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDUxMDU0NTgsImV4cCI6MjAyMDY4MTQ1OH0.bPEC27l-BgdJyXC6HCS4FqEzPl4hjaqumP5Ltd93Wuk',
  );
  /*await Supabase.initialize(
    url: 'http://85.215.56.129:3000',
    anonKey:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ewogICJyb2xlIjogImFub24iLAogICJpc3MiOiAic3VwYWJhc2UiLAogICJpYXQiOiAxNzA1MDE0MDAwLAogICJleHAiOiAxODYyODY2ODAwCn0.CxbhtiNQjiLnzzbh4ikSeI03x2YC3kb5kcsr6DzsGEg',
  );*/

  try {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);
  } catch (e) {
    print(e);
  }
  await PlayerData.loadPlayerDataFromOnline();
  runApp(
    MultiProvider(
      // Extra Provider für Profil und Comic Sans Mode
      providers: [
        ChangeNotifierProvider(create: (context) => UserProfileProvider()),
        ChangeNotifierProvider(create: (context) => ThemeProvider()),
      ],
      child: const Memeorize(),
    ),
  );
}

/// Provider für das Profil und Comic Sans Mode
class ThemeProvider extends ChangeNotifier {
  bool _isComicSansMode = false;

  bool get isComicSansMode => _isComicSansMode;

  void toggleComicSansMode() {
    _isComicSansMode = !_isComicSansMode;
    notifyListeners();
  }

  void setComicSansMode(bool isOn) {
    if (_isComicSansMode != isOn) {
      _isComicSansMode = isOn;
      notifyListeners();
    }
  }
}

//Theme
class Memeorize extends StatelessWidget {
  const Memeorize({super.key});

  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<ThemeProvider>(context);
    Color textColor = themeProvider.isComicSansMode
        ? const Color.fromARGB(255, 255, 0, 255)
        : Colors.white;
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'MEMEORIZE - Let\'s Meme!',
        theme: ThemeData(
          fontFamily:
              // Wenn Comic Sans Mode an ist, dann verwende Comic Sans
              themeProvider.isComicSansMode ? 'Comic_Sans' : 'Montserrat',
          canvasColor: MemeBlackGreen,
          cardColor: MemeBlackGreen,
          iconTheme: const IconThemeData(
              color: Color.fromARGB(255, 255, 255, 255)), //NOTWORKING
          colorScheme: ColorScheme.fromSeed(
              seedColor: const Color.fromARGB(
                  255, 7, 7, 7), //Background Color / Menu links, Schwarz
              primary: const Color.fromARGB(255, 14, 18, 20), //Background, Grau
              onPrimary: const Color.fromARGB(
                  255, 30, 32, 32), //Highlighted Element, leichtes Grau
              secondary:
                  const Color.fromARGB(255, 255, 255, 255), //Textfarbe, weiß
              onSecondary: const Color.fromARGB(
                  255, 221, 255, 0)), //Highlited Text / Hauptfarbe, Grün

          buttonTheme: ButtonThemeData(
            buttonColor: MemeBlackGreen,
            textTheme: ButtonTextTheme.primary,
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(3.0), // Leicht abgerundete Ecken
            ),
          ),
          textTheme: TextTheme(
            headlineLarge: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
            headlineMedium: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
            headlineSmall: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
            titleLarge: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
            bodyLarge: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w300,
              color: textColor,
            ),
            bodyMedium: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w300,
              color: textColor,
            ),
            bodySmall: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w300,
              color: textColor,
            ),
          ),
        ),
        home: const MemeorizeHome());
  }
}

class MemeorizeHome extends StatefulWidget {
  const MemeorizeHome({super.key});

  @override
  MemeorizeHomeState createState() => MemeorizeHomeState();
}

class MemeorizeHomeState extends State<MemeorizeHome> {
  final _passwordController = TextEditingController();
  final List<Widget> pages = [
//---------------------- // Startseite \\ ----------------------

    const Homepage_Firebase(),

//---------------------- // Hinzufügen Tab \\ ----------------------

    const AddMeme(),

//---------------------- // Memeollection \\ ----------------------

    const Memeollection(),

//---------------------- // Freunde oder Feinde \\ ----------------------
    const Friends()
  ];
  int selectedPage = 0;
  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    return Scaffold(
//---------------------- // MOBILE VIEW \\ ----------------------
      bottomNavigationBar: MediaQuery.of(context).size.width < 800
          ? SizedBox(
              height: 80,
              child: BottomNavigationBar(
                  backgroundColor: Colors.black,
                  type: BottomNavigationBarType.fixed,
                  currentIndex: selectedPage,
                  unselectedItemColor: const Color.fromARGB(255, 255, 255, 255),
                  selectedItemColor: const Color.fromARGB(255, 221, 255, 0),
                  selectedLabelStyle: TextStyle(
                    fontFamily: themeProvider.isComicSansMode
                        ? 'Comic_Sans'
                        : 'Montserrat',
                  ),
                  unselectedLabelStyle: TextStyle(
                    fontFamily: themeProvider.isComicSansMode
                        ? 'Comic_Sans'
                        : 'Montserrat',
                  ),
                  onTap: (int index) {
                    if (index == 1) {
                      // Wenn der "AddMeme"-Button geklickt wird
                      _navigateToAddMeme(context);
                    } else {
                      setState(() {
                        selectedPage = index;
                      });
                    }
                  },
                  items: const [
                    BottomNavigationBarItem(
                        icon: Icon(
                          Icons.play_arrow,
                        ),
                        label: "Let's Meme!"),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.add_box), label: "Hinzufügen"),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.grid_view), label: "Memeollection"),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.people), label: "Friends"),
                  ]),
            )
          : null,
//---------------------- // DESKTOPVIEW \\ ----------------------
      body: Row(
        children: <Widget>[
          //Switch Navigation, wenn kleiner als 800 ist
          if (MediaQuery.of(context).size.width >= 800)
            LayoutBuilder(builder: (context, constraints) {
              return SingleChildScrollView(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints(minHeight: constraints.maxHeight),
                      child: IntrinsicHeight(
                          child: NavigationRail(
                              indicatorShape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3))),
                              indicatorColor: MemeGreen,
                              unselectedIconTheme: const IconThemeData(
                                  color: Color.fromARGB(255, 255, 255, 255)),
                              selectedIconTheme: const IconThemeData(
                                color: Colors.black,
                              ),
                              extended: true,
                              labelType: NavigationRailLabelType.none,
                              backgroundColor:
                                  const Color.fromARGB(255, 7, 7, 7),
                              onDestinationSelected: (int index) {
                                if (index == 1) {
                                  // Wenn der "AddMeme"-Button ausgewählt wird
                                  _navigateToAddMeme(context);
                                } else {
                                  setState(() {
                                    selectedPage = index;
                                  });
                                }
                              },
                              selectedIndex: selectedPage,
                              destinations: const [
                                NavigationRailDestination(
                                    icon: Icon(
                                      Icons.play_arrow,
                                    ),
                                    label: Text("Let's Meme!")),
                                NavigationRailDestination(
                                    icon: Icon(Icons.add_box),
                                    label: Text("Hinzufügen")),
                                NavigationRailDestination(
                                    icon: Icon(
                                      Icons.grid_view,
                                    ),
                                    label: Text("Memeollection")),
                                NavigationRailDestination(
                                    icon: Icon(
                                      Icons.people,
                                    ),
                                    label: Text("Friends")),
                              ],
                              selectedLabelTextStyle: TextStyle(
                                color: const Color(0xFFDDFF00),
                                fontFamily: themeProvider.isComicSansMode
                                    ? 'Comic_Sans'
                                    : 'Montserrat',
                              ),
                              unselectedLabelTextStyle: TextStyle(
                                color: const Color.fromARGB(255, 255, 255, 255),
                                fontFamily: themeProvider.isComicSansMode
                                    ? 'Comic_Sans'
                                    : 'Montserrat',
                              ),
                              leading: const Column(
                                children: [
                                  SizedBox(height: 20),
                                  Image(
                                      width: 175,
                                      image: AssetImage(
                                          'assets/images/MEMORIZE_Logo.png')),
                                  SizedBox(height: 20)
                                ],
                              ),
                              trailing: Column(
                                children: [
                                  const SizedBox(height: 25),
                                  const SizedBox(height: 30),
                                  RankingBar(
                                    widgetWidth: 300,
                                    scaleAutomatically: true,
                                  ),
                                  TextButton(
                                    onPressed: () => {_showMyDialog(context)},
                                    style: TextButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(3.0)),
                                        foregroundColor: Colors.white,
                                        backgroundColor: MemeDarkGrey),
                                    child: const Text("Authors"),
                                  ),
                                  const SizedBox(height: 30),
                                ],
                              )))));
            }),
          //Felix seine Ranking Liste
          Expanded(child: pages[selectedPage])
        ],
      ),
    );
  }

  // Charlotte:
  // Dialog für die Eingabe des Admin-Passworts, um
  // die AddMeme-Seite zu öffnen
  void _navigateToAddMeme(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.black,
        title: const Text('Add Meme', style: TextStyle(color: Colors.white)),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Please enter the admin password to add a meme.',
              style: TextStyle(color: Colors.white),
            ),
            Container(
              height: 20,
            ),
            AdminPasswordField(passwordController: _passwordController),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel', style: TextStyle(color: Colors.white)),
          ),
          TextButton(
            onPressed: () {
              if (!AdminPasswordField.isAllowedToCheckPassword(context)) {
                return;
              }
              if (_passwordController.text !=
                  AdminPasswordField.adminPassword) {
                print("wrong password");
                AdminPasswordField.showPasswordFailedDialog(context);
                return;
              }
              Navigator.of(context).pop();
              setState(() {
                selectedPage = 1; // Index der AddMeme-Seite
              });
            },
            child: const Text('Submit',
                style: TextStyle(
                  color: Color.fromARGB(255, 219, 255, 0),
                )),
          ),
        ],
      ),
    );
  }

//kleiner "About"-Popup, damit man sieht, wer die Anwendung gemacht hat
  Future<void> _showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.black,
          title: const Center(
            child: Text(
              "Authors",
              style: TextStyle(color: Colors.white),
            ),
          ),
          content: const SingleChildScrollView(
              child: Center(
            child: ListBody(
              children: <Widget>[
                SizedBox(height: 25),
                Center(
                  child: Text(
                    "Team 1 - Team Marcel!",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(height: 25),
                Center(
                  child: Text(
                    "Hendrik Oostinga",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Center(
                  child: Text(
                    "Felix Langrehr",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Center(
                  child: Text(
                    "Marcel Nema",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Center(
                  child: Text(
                    "Charlotte Bärhold",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Center(
                  child: Text(
                    "Nele Schulze",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(height: 25),
                Spacer(flex: 5)
              ],
            ),
          )),
          actions: <Widget>[
            Center(
              child: OutlinedButton(
                child: const Text(
                  "All right!",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            )
          ],
        );
      },
    );
  }
}
