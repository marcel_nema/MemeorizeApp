import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memeorize/ForbiddenWords.dart';
import 'package:memeorize/pages/profile.dart';
import 'package:memeorize/player_level.dart';
import 'package:memeorize/utils/model.dart';
import 'package:memeorize/utils/utils.dart';
import 'package:provider/provider.dart';
import '../style.dart';

/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hier wird man von der profile.dart hingeleitet, wenn man auf den EDIT-Button klickt, 
und kann somit Name, E-Mail und Foto ändern. Klickt man auf SAVE, wird man wieder zu 
profile.dart also zum Profile-Widget zurück navigiert. Die geänderten Daten werden in 
Firebase gespeichert.
*/ //     --------------------------------//\\--------------------------------

class EditProfilePage extends StatefulWidget {
  final UserProfile initialProfile;
  const EditProfilePage(
      {super.key, required this.initialProfile, required this.title});

  final String title;

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  Uint8List? _image;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  Uint8List? newImage;

  @override
  void initState() {
    super.initState();
    //initialize controllers with current user profile data
    UserProfile userProfile =
        Provider.of<UserProfileProvider>(context, listen: false).userProfile;
    nameController.text = userProfile.name;
    emailController.text = userProfile.email;
  }

//Bild mit Imagepicker aus Gallerie auswählen
  void selectImage() async {
    Uint8List img =
        await pickImage(ImageSource.gallery); //Zugriff auf utils.dart
    setState(() {
      _image = img;
    });
  }

//Name und Email speichern + Navigation zurück zum Profil-Screen
  void saveProfile() {
    String name = nameController.text;
    String email = emailController.text;

    //String resp = await StoreData().saveData(name: name, email: email, file: _image!);

    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ProfilePage(
              title: 'title',
              initialProfile:
                  UserProfile(name: name, email: email, image: _image),
            )));
  }

  //pass updated profile data back to 'ProfilePage' when changes confirmed
  void saveChanges() {
    UserProfile updatedProfile = UserProfile(
        name: nameController.text,
        email: emailController.text,
        image: newImage);

    Navigator.pop(context, updatedProfile);
  }

  @override
  //Profil-ändern-Widget in dem Foto, Name und Email geändert werden können
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MemeBlackGreen,
        body: Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 80),
            child: Column(children: [
              Stack(
                children: [
                  //schwarzer basis-Container
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: Container(
                      height: 150,
                      width: 500,
                      color: Colors.black,
                    ),
                  ),
                  //Foto
                  _image != null
                      ? Positioned(
                          top: 10,
                          left: 10,
                          child: SizedBox(
                            height: 130,
                            width: 130,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5.0),
                              child: Image.memory(
                                _image!,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ))
                      : Positioned(
                          top: 10,
                          left: 10,
                          child: SizedBox(
                            height: 130,
                            width: 130,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(5.0),
                                child: ColorFiltered(
                                    colorFilter: const ColorFilter.mode(
                                        MemeGreen, BlendMode.color),
                                    child: Image.asset(
                                      'assets/images/hide-pain-harold.jpg',
                                      fit: BoxFit.cover,
                                    ))),
                          ),
                        ),
                  Positioned(
                    bottom: 40,
                    left: 40,
                    child: IconButton(
                        onPressed: selectImage,
                        icon: const Icon(
                          Icons.add_a_photo_outlined,
                          color: Colors.white,
                          size: 50,
                        )),
                  ),
                  //Name eingeben
                  Positioned(
                      top: 10,
                      left: 150,
                      child: Container(
                          width: 300,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          child: TextField(
                            controller: nameController,
                            decoration: InputDecoration(
                                hintText: 'Enter name',
                                hintStyle: const TextStyle(color: Colors.white),
                                border: OutlineInputBorder(
                                    borderSide:
                                        const BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(5))),
                          ))),
                  //Email eingeben
                  Positioned(
                      top: 60,
                      left: 150,
                      child: Container(
                          width: 300,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          child: TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                                hintText: 'Enter email',
                                hintStyle: const TextStyle(color: Colors.white),
                                border: OutlineInputBorder(
                                    borderSide:
                                        const BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(5))),
                          ))),
                  //SAVE-Button updaten der Profil-Daten
                  Positioned(
                    bottom: 10,
                    right: 10,
                    child: ElevatedButton(
                      onPressed: () {
                        if (!ForbiddenWords.isNameOkay(nameController.text)) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  backgroundColor: Colors.red,
                                  title: Text(
                                    "The name you entered contains forbidden words.",
                                    style: MemeFont,
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text("OK"),
                                    )
                                  ],
                                );
                              });
                          return;
                        }
                        UserProfileProvider userProfileProvider =
                            Provider.of<UserProfileProvider>(context,
                                listen: false);

                        //Update user profile
                        userProfileProvider.updateUserProfile(UserProfile(
                            name: nameController.text,
                            email: emailController.text,
                            image: _image));
                        //PlayerData.loadPlayerData();
                        PlayerData.playerName = nameController.text;
                        PlayerData.savePlayerData();

                        //Navigate back
                        Navigator.pop(context);
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: const Color.fromARGB(255, 219, 255, 0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        minimumSize: const Size(
                            0, 30), // Set the minimum size for the button
                      ),
                      child: const Text(
                        'SAVE',
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              )
            ]),
          ),
        ));
  }
}
