import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memeorize/admin_password_field.dart';
import 'package:memeorize/firebaseservice.dart';
import 'package:memeorize/pages/edit_profile.dart';
import 'package:memeorize/player_level.dart';
import 'package:memeorize/ranking_bar.dart';
import 'package:memeorize/style.dart';
import 'package:memeorize/utils/model.dart';
import 'package:provider/provider.dart';
import 'dart:ui';

/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hier wird das Profil gebaut, dabei werden die Daten (Name, Email und Foto) in 
Firebase gespeichert und von dort hierher geladen. Somit wird es z.B. auch dann 
gespeichert, wenn man einen anderen Tab der App öffnet. Um das Profil zu ändern, 
wird man zur EditProfilePage (in edit_profile.dart) weitergeleitet. Außerdem werden
die Daten des ursprünglichen Profils über model.dart angezeigt und dort geupdated.
*/ //     --------------------------------//\\--------------------------------

class ProfilePage extends StatefulWidget {
  final UserProfile initialProfile;
  const ProfilePage(
      {super.key, required this.initialProfile, required this.title});

  final String title;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin {
  late UserProfileProvider _userProfileProvider;
  late UserProfile _userProfile;
  bool passcodeHidden = true;
  TextEditingController _uidController = TextEditingController();
  TextEditingController _passcodeController = TextEditingController();
  FirebaseService _firebaseService = FirebaseService();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _userProfile = widget.initialProfile;
  }

  Uint8List? _image;

//Navigation zur edit_profile.dart
  void editProfile() async {
    var updatedProfile =
        await Navigator.of(context).push<UserProfile>(MaterialPageRoute(
            builder: (context) => EditProfilePage(
                  initialProfile: UserProfile(
                      name: 'Current Name',
                      email: 'Current Email',
                      image: _image),
                  title: 'title',
                )));

    // aktualisiere das Profil
    if (updatedProfile != null) {
      setState(() {
        _image = updatedProfile.image;
      });
    }
  }

  @override
  //Profil-Widget in dem Foto, Name und Email geändert werden können
  Widget build(BuildContext context) {
    _userProfileProvider = Provider.of<UserProfileProvider>(context);

    return Scaffold(
      backgroundColor: MemeBlackGreen,
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height +
              clampDouble(934 - MediaQuery.of(context).size.height, 0, 300) +
              100,
          child: Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: Column(children: [
                    Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Container(
                            height: 150,
                            width: 500,
                            color: Colors.black,
                          ),
                        ),
                        _image != null
                            ? Positioned(
                                top: 10,
                                left: 10,
                                child: SizedBox(
                                  height: 130,
                                  width: 130,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5.0),
                                    child: Image.memory(
                                      _image!,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ))
                            : Positioned(
                                top: 10,
                                left: 10,
                                child: SizedBox(
                                  height: 130,
                                  width: 130,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(5.0),
                                      child: _userProfileProvider
                                                  .userProfile.image !=
                                              null
                                          ? Image.memory(
                                              _userProfileProvider
                                                  .userProfile.image!,
                                              fit: BoxFit.cover,
                                            )
                                          : Image.asset(
                                              "assets/images/hide-pain-harold.jpg",
                                              fit: BoxFit.cover,
                                            )),
                                ),
                              ),
                        //Name + Email
                        Positioned(
                            top: 20,
                            left: 170,
                            child: Column(
                              children: [
                                Consumer<UserProfileProvider>(
                                    builder: (context, UserProfileProvider, _) {
                                  return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          UserProfileProvider.userProfile.name,
                                          style: const TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 26),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text("E-MAIL: ",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headlineLarge!
                                                    .copyWith(fontSize: 16)),
                                            Text(
                                                UserProfileProvider
                                                    .userProfile.email,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headlineLarge!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        fontSize: 16)),
                                          ],
                                        ),
                                      ]);
                                })
                              ],
                            )),
                        //Edit-Button -> Navigation zur editprofilepage
                        Positioned(
                          bottom: 10,
                          right: 10,
                          child: ElevatedButton(
                            key: const Key('edit_button_key'),
                            onPressed: editProfile,
                            style: ElevatedButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor:
                                  const Color.fromARGB(255, 219, 255, 0),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              minimumSize: const Size(0, 30),
                            ),
                            child: const Text(
                              'EDIT',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black),
                            ),
                          ),
                        ),
                      ],
                    )
                  ]),
                ),
              ),
              Container(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Column(
                    children: [
                      Text(
                        "UID:",
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 150,
                        height: 10,
                      ),
                      Text(
                        "Code:",
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        width: 330,
                        height: 20,
                        child: Row(
                          children: [
                            Text(
                              PlayerData.userID,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                            IconButton(
                              onPressed: () async {
                                await Clipboard.setData(
                                    ClipboardData(text: PlayerData.userID));
                              },
                              icon: Icon(Icons.copy),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(left: 20, right: 20),
                              constraints: BoxConstraints(),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: 330,
                        height: 20,
                        child: Row(
                          children: [
                            Text(
                              passcodeHidden
                                  ? "*******************"
                                  : UserDataEncoder.encodeUID(
                                      PlayerData.userID),
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                setState(() {
                                  passcodeHidden = !passcodeHidden;
                                });
                              },
                              icon: Icon(Icons.visibility_off),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(left: 20, right: 20),
                              constraints: BoxConstraints(),
                            ),
                            IconButton(
                              onPressed: () async {
                                await Clipboard.setData(ClipboardData(
                                    text: UserDataEncoder.encodeUID(
                                        PlayerData.userID)));
                              },
                              icon: Icon(Icons.copy),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(left: 20, right: 20),
                              constraints: BoxConstraints(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                height: 20,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                      foregroundColor: Colors.white,
                      backgroundColor: MemeDarkGrey,
                    ),
                    onPressed: () => _dialogBuilder(context),
                    child: Text("Change User"),
                  ),
                  IconButton(
                    onPressed: () async {
                      if (PlayerData.userID == null ||
                          PlayerData.userID == "") {
                        return;
                      }
                      RetrievePlayerData? retData =
                          await _firebaseService.getSpecificPlayersSupabase(
                              PlayerData.userID,
                              UserDataEncoder.encodeUID(PlayerData.userID));
                      if (retData == null || retData.userID == "") {
                        return;
                      }
                      PlayerData.userID = retData.userID;
                      PlayerData.level = retData.level;
                      PlayerData.experience = retData.experience;
                      PlayerData.playerName = retData.playerName;
                      PlayerData.savePlayerData();
                      PlayerData.addExperience(0);
                      Navigator.of(context).pop();
                      setState(() {});
                    },
                    icon: Icon(Icons.refresh),
                    tooltip: "Refresh current user's data",
                  ),
                ],
              ),
              Container(
                height: 20,
              ),
              RankingBar(
                widgetWidth: MediaQuery.of(context).size.width * 0.8,
                widgetHeight: MediaQuery.of(context).size.height * 0.7,
                fontSizeMultiplier:
                    1 + MediaQuery.of(context).size.width * 0.0003,
                backgroundColor: const Color(0xFF0E1214),
                buttonHeight: MediaQuery.of(context).size.height * 0.04,
                buttonWidth: MediaQuery.of(context).size.width * 0.3,
                expWidth: MediaQuery.of(context).size.width * 0.6,
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _dialogBuilder(BuildContext context) {
    _uidController.text = "";
    _passcodeController.text = "";
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: MemeBlackGreen,
          title: const Text('Change User'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text('To change user, you need your UserID (UID)\n'
                  'and your Passcode (Code) that you can find on the \n'
                  'profile page from the Account that you want to use.\n'),
              TextFormField(
                obscureText: false,
                controller: _uidController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "UserID (UID)",
                  fillColor: Colors.black,
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                style: const TextStyle(color: Colors.white),
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                obscureText: true,
                controller: _passcodeController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Passcode (Code)",
                  fillColor: Colors.black,
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                style: const TextStyle(color: Colors.white),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text(
                'Cancel',
                style: TextStyle(color: MemeGrey),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text(
                'Change User',
                style: TextStyle(color: MemeGreen),
              ),
              onPressed: () async {
                RetrievePlayerData? retData =
                    await _firebaseService.getSpecificPlayersSupabase(
                        _uidController.text, _passcodeController.text);
                if (retData == null || retData.userID == "") {
                  return;
                }
                PlayerData.userID = retData.userID;
                PlayerData.level = retData.level;
                PlayerData.experience = retData.experience;
                PlayerData.playerName = retData.playerName;
                PlayerData.savePlayerData();
                PlayerData.addExperience(0);
                Navigator.of(context).pop();
                setState(() {});
              },
            ),
          ],
        );
      },
    );
  }
}
