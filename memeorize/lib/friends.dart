import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:memeorize/pages/profile.dart';
import 'package:memeorize/utils/model.dart';

/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hier wird der Teil der "Profile"-Seite gebaut, in dem sich die Nutzerdaten befinden.
Der Übersicht halber ist hier nur der build, der Rest des Codes ist in die Dateien der
Ordner pages und utils ausgelagert.
*/ //     --------------------------------//\\--------------------------------
class Friends extends StatelessWidget {
  const Friends({
    super.key,
  });

  @override
  //baut das Profil-Widget
  Widget build(BuildContext context) {
    Uint8List? image;
    return Scaffold(
        body: ProfilePage(
      title: 'Register Profile',
      initialProfile: UserProfile(
          name: 'Current Name', email: 'Current Email', image: image),
    ));
  }
}
