/*
@Autorin: Charlotte

Diese Datei enthält die Klasse Memeollection, die die Memesammlung in einem Gridview anzeigt.
Hovert man über ein Meme, verdunkelt sich dieses und ein Delete- und Edit-Button erscheinen.
Der Delete-Button löscht das Meme, der Edit-Button öffnet die EditMeme-Seite (editmeme.dart) 
mit den Daten des Memes.
Klickt man auf die HoverCard, öffnet sich ein Popup, das das Meme in groß anzeigt sowie den 
Titel und (wenn vorhanden) die Beschreibung.
Über "Report Meme" wird das Meme gemeldet, d.h. es wird in der Datenbank ein neuer Report-Eintrag
erstellt, der den Titel und die ID des Memes enthält sowie den Timestamp, wann das Meme gemeldet wurde.
Über die Suchleiste kann nach Titeln gesucht werden, die den Suchbegriff enthalten. 

Hinzufügen der Admin-Einstellung zum Löschen und Bearbeiten von @Felix.

--------------------------------------------------------------------------------*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hovering/hovering.dart';
import 'package:memeorize/firebaseservice.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memeorize/style.dart';
import 'admin_password_field.dart';
import 'editmeme.dart';

class Memeollection extends StatefulWidget {
  const Memeollection({super.key});

  @override
  _MemeollectionState createState() => _MemeollectionState();
}

class _MemeollectionState extends State<Memeollection> {
  final TextEditingController _searchController = TextEditingController();
  final _passwordController = TextEditingController();
  List<DocumentSnapshot> _allMemes = [];
  List<DocumentSnapshot> _filteredMemes = [];
  // DocumentSnapshot ist ein Objekt, das ein Dokument aus der Datenbank repräsentiert
  final FirebaseService _firebaseService = FirebaseService();

  @override
  void initState() {
    super.initState();
    _loadMemes();
    _searchController.addListener(() {
      _filterMemes();
    });

    // Sucht nach Memes, die den Suchbegriff enthalten und aktualisiert die Memesammlung entsprechend
    // (wird jedes Mal aufgerufen, wenn sich der Suchbegriff ändert)
    _firebaseService.getMemesAsStream().listen((QuerySnapshot snapshot) {
      var filteredMemes = snapshot.docs.where((DocumentSnapshot document) {
        return document['title']
            .toString()
            .toLowerCase()
            .contains(_searchController.text.toLowerCase());
        // contains() gibt true zurück, wenn der Suchbegriff in dem String enthalten ist
      }).toList();

      if (mounted) {
        // Nur aktualisieren, wenn das Widget sichtbar ist, sonst gibt es einen Fehler
        setState(() {
          _filteredMemes = filteredMemes;
        });
      }
    });
  }

  @override
  // Textfelder müssen manuell entsorgt werden, sonst gibt es einen Fehler
  void dispose() {
    _searchController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  // Lädt alle Memes aus der Datenbank
  Future<void> _loadMemes() async {
    _firebaseService.getMemesAsStream().listen((QuerySnapshot snapshot) {
      _allMemes = snapshot.docs;
      _filterMemes();
    });
  }

  // Sucht nach Memes, die den Suchbegriff enthalten und aktualisiert die Memesammlung entsprechend
  void _filterMemes() {
    final searchQuery = _searchController.text.toLowerCase();
    List<DocumentSnapshot> newFilteredMemes = _allMemes.where((doc) {
      // Bedingung, die für jedes Element in der Liste geprüft wird
      String title = doc['title'].toLowerCase();
      return title
          .contains(searchQuery); // true, wenn der Suchbegriff enthalten ist
    }).toList();
    if (mounted) {
      // Nur aktualisieren, wenn das Widget sichtbar ist, sonst gibt es einen Fehler
      setState(() {
        _filteredMemes = newFilteredMemes;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemeBlackGreen,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: MediaQuery.of(context).size.height * 0.05,
                ),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width / 2.5,
                  child: TextField(
                    cursorColor: Colors.white,
                    controller:
                        _searchController, // Controller für das Suchfeld
                    style: const TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Search Memes',
                      hintStyle: const TextStyle(color: Colors.white),
                      prefixIcon: const Padding(
                        padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                        child: Icon(Icons.search, color: Colors.white),
                      ),
                      suffixIcon: _searchController.text.isNotEmpty
                          ? IconButton(
                              icon:
                                  const Icon(Icons.clear, color: Colors.white),
                              onPressed: () {
                                _searchController.clear();
                              },
                            )
                          : null, // Only show the icon when the search field is not empty
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(200.0),
                        borderSide: BorderSide.none,
                      ),
                      filled: true,
                      fillColor: Colors.black,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: GridView.builder(
                  // Ein GridView mit 4 Spalten
                  // Jedes Element ist eine MemeCard
                  // OnHover wird die HoverCard angezeigt
                  itemCount: _filteredMemes.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount:
                        MediaQuery.of(context).size.width < 1000 ? 2 : 4,
                  ),
                  itemBuilder: (context, index) {
                    // itemBuilder erstellt jeweils ein Element des Grids
                    final DocumentSnapshot memeData = _filteredMemes[index];
                    return HoverWidget(
                      hoverChild: buildHoverCard(memeData, context),
                      // hoverChild ist das Widget, das angezeigt wird, wenn über das Element gehovert wird
                      onHover: (PointerEnterEvent event) {},
                      child: buildMemeCard(memeData),
                      // child ist das Widget, das angezeigt wird, wenn nicht über das Element gehovert wird
                    );
                  },
                ),
              ),
            ],
          ),
          Positioned(
            // Erstellt Fadeout am unteren Rand
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: MediaQuery.of(context).size.height / 12,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color.fromARGB(255, 14, 18, 20),
                    Colors.transparent,
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Ein eigenes Widget für die MemeCard für bessere Übersichtlichkeit
  Widget buildMemeCard(DocumentSnapshot memeData) {
    return Card(
      child: FractionallySizedBox(
        // macht das Bild responsive,
        // d.h. es passt sich der Größe des Containers an
        child: Container(
          color: MemeDarkGrey,
          child: ClipRRect(
            // ClipRRect macht die Ecken des Bildes rund
            borderRadius: BorderRadius.circular(12.0),
            child: Image.network(
              // Image.network lädt das Bild aus der URL
              memeData['imageUrl'],
              fit: BoxFit.cover,
              // Füllt den Container mit dem Bild aus
            ),
          ),
        ),
      ),
    );
  }

  // Ein eigenes Widget für die HoverCard, damit die HoverCard nicht in der MemeCard verschwindet
  // Die Hovercard verdunkelt die Memes und zeigt einen Delete- und Edit-Button an sowie den Titel
  Widget buildHoverCard(DocumentSnapshot memeData, BuildContext context) {
    final double imageSize =
        MediaQuery.of(context).size.width / 4; // 4 Bilder in einer Reihe
    final double iconSize = imageSize / 4; // 1/4 der Bildgröße

    return InkWell(
      // Inkwell macht das Widget anklickbar
      onTap: () {
        showDialog(
          context: context,
          builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            backgroundColor: Colors.black,
            child: SizedBox(
              width: 800,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(Icons.close, color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12.0),
                            child: Image.network(
                              memeData['imageUrl'],
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 20, 8, 8),
                          child: Text(
                            memeData['title'],
                            style: Theme.of(context).textTheme.headlineLarge,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(30, 8, 30, 8),
                          child: Text(
                            memeData['info'] ??
                                'No further information is available.',
                            style: const TextStyle(
                                color: Colors.white, fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: ReportMemeDialog(
                            memeId: memeData.id,
                            memeTitle: memeData['title'],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
      child: Card(
        clipBehavior: Clip.antiAlias, // Runde Ecken
        child: HoverContainer(
          width: imageSize,
          height: imageSize,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(memeData['imageUrl']),
              fit: BoxFit.cover,
              // Füllt den Container mit dem Bild aus
              // Dunkelt das Bild ab, damit der Text besser lesbar ist
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.all(iconSize / 4),
                child: Text(
                  memeData['title'],
                  style: Theme.of(context).textTheme.headlineMedium,
                  textAlign: TextAlign.center,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Padding(
                    padding: EdgeInsets.all(iconSize / 4),
                    child: ColorChangingIconWithBorder(
                      iconData: Icons.delete,
                      defaultColor: Colors.white,
                      hoverColor: Colors.red,
                      onPressed: () {
                        showDialog(
                          // Confirmation Dialog
                          context: context,
                          builder: (context) => AlertDialog(
                            backgroundColor: Colors.black,
                            title: const Text('Delete Meme',
                                style: TextStyle(color: Colors.white)),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Text(
                                  'Are you sure you want to delete this meme?',
                                  style: TextStyle(color: Colors.white),
                                ),
                                Container(
                                  height: 20,
                                ),
                                // Textfeld für das Admin-Passwort (siehe admin_password_field.dart)
                                AdminPasswordField(
                                    passwordController: _passwordController),
                              ],
                            ),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('Cancel',
                                    style: TextStyle(color: Colors.white)),
                              ),
                              TextButton(
                                onPressed: () {
                                  if (!AdminPasswordField
                                      .isAllowedToCheckPassword(context)) {
                                    return;
                                  }
                                  if (_passwordController.text !=
                                      AdminPasswordField.adminPassword) {
                                    print("Falsches Passwort");
                                    AdminPasswordField.showPasswordFailedDialog(
                                        context);
                                    return;
                                  }
                                  _firebaseService.deleteMeme(memeData.id);
                                  Navigator.pop(context);
                                },
                                child: const Text('Delete',
                                    style: TextStyle(color: Colors.red)),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(iconSize / 4),
                    child: ColorChangingIconWithBorder(
                      iconData: Icons.edit,
                      defaultColor: Colors.white,
                      hoverColor: const Color.fromARGB(255, 221, 255, 0),
                      onPressed: () {
                        _showEditDialog(memeData);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Ein eigenes Widget für den Edit-Button,
  // damit der Dialog nicht in der HoverCard verschwindet.
  // Der Dialog fragt nach dem Admin-Passwort
  // und öffnet die EditMeme-Seite (editmeme.dart)
  void _showEditDialog(DocumentSnapshot memeData) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.black,
        title: const Text('Edit Meme', style: TextStyle(color: Colors.white)),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Please enter the admin password to edit this meme.',
              style: TextStyle(color: Colors.white),
            ),
            Container(
              height: 20,
            ),
            AdminPasswordField(passwordController: _passwordController),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel', style: TextStyle(color: Colors.white)),
          ),
          TextButton(
            onPressed: () {
              if (!AdminPasswordField.isAllowedToCheckPassword(context)) {
                return;
              }
              if (_passwordController.text !=
                  AdminPasswordField.adminPassword) {
                print("Falsches Passwort");
                AdminPasswordField.showPasswordFailedDialog(context);
                return;
              }
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditMeme(
                    quizData: memeData,
                  ),
                ),
              );
            },
            child: const Text('Edit',
                style: TextStyle(
                  color: Color.fromARGB(255, 219, 255, 0),
                )),
          ),
        ],
      ),
    );
  }
}

// Eine eigene Klasse für den Report-Button, damit der Dialog nicht in der HoverCard verschwindet
class ReportMemeDialog extends StatelessWidget {
  final String memeId;
  final String memeTitle;

  const ReportMemeDialog({
    super.key,
    required this.memeId,
    required this.memeTitle,
  });

  Future<void> reportMeme() async {
    final reportsCollection = FirebaseFirestore.instance.collection('reports');
    await reportsCollection.add({
      'memeId': memeId,
      'memeTitle': memeTitle,
      'reportedAt': FieldValue
          .serverTimestamp(), // optional, fügt einen Zeitstempel hinzu
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            backgroundColor: Colors.black,
            title: const Text('Report Meme',
                style: TextStyle(color: Colors.white)),
            content: const Text('Are you sure you want to report this meme?',
                style: TextStyle(color: Colors.white)),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child:
                    const Text('Cancel', style: TextStyle(color: Colors.white)),
              ),
              TextButton(
                onPressed: () {
                  reportMeme();
                  Navigator.pop(context);
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      backgroundColor: Colors.black,
                      title: const Text('Report Meme',
                          style: TextStyle(color: Colors.white)),
                      content: const Text(
                          'Your report has been sent successfully.',
                          style: TextStyle(color: Colors.white)),
                      actions: [
                        TextButton(
                          onPressed: () => Navigator.pop(context),
                          child: const Text('Ok',
                              style: TextStyle(color: Colors.white)),
                        ),
                      ],
                    ),
                  );
                },
                child:
                    const Text('Report', style: TextStyle(color: Colors.red)),
              ),
            ],
          ),
        );
      },
      child: const Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Text('Report Meme', style: TextStyle(color: Color(0xFF5C5D61))),
          Icon(Icons.flag, color: Color(0xFF5C5D61), size: 20),
        ],
      ),
    );
  }
}

// Eine Klasse für den Delete- und Edit-Button auf der HoverCard
// Wenn über den Button gehovert wird, ändert sich die Farbe
class ColorChangingIconWithBorder extends StatefulWidget {
  final IconData iconData;
  final Color defaultColor;
  final Color hoverColor;
  final VoidCallback? onPressed;

  const ColorChangingIconWithBorder({
    super.key,
    required this.iconData,
    required this.defaultColor,
    required this.hoverColor,
    this.onPressed,
  });

  @override
  _ColorChangingIconWithBorderState createState() =>
      _ColorChangingIconWithBorderState();
}

class _ColorChangingIconWithBorderState
    extends State<ColorChangingIconWithBorder> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPressed,
      onHover: (hovered) {
        setState(() {
          isHovered = hovered;
        });
      },
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          border: Border.all(
            color: isHovered ? widget.hoverColor : widget.defaultColor,
            width: 2.0,
          ),
        ),
        child: Center(
          child: Icon(
            widget.iconData,
            color: isHovered ? widget.hoverColor : widget.defaultColor,
          ),
        ),
      ),
    );
  }
}
