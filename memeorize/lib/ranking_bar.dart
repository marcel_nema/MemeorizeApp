/*

Autor dieser Datei:
Felix Langrehr

Dient hauptsächlich zum Anzeigen der Ranking-Liste mit
ausgedachten "Gegnern", um den Spieler zu ermutigen,
diese zu übertreffen.
Nebenbei enthält sie auch die Spielerdaten (damit ist 
nicht nur der spielende Spieler, sondern auch die 
simulierten Gegner gemeint), die fürdas Ranking 
relevant sind (EXP, Level, Rang).

*/
import 'package:flutter/material.dart';
import 'package:memeorize/firebaseservice.dart';
import 'player_level.dart';
import 'style.dart';

class RankingBar extends StatefulWidget {
  double widgetWidth = 300;
  double widgetHeight = 450;
  double fontSizeMultiplier = 1.0;
  double titleFontSizeMultiplier = 1.0;
  double buttonWidth = 100.0;
  double buttonHeight = 20.0;
  double expWidth = 200.0;
  bool scaleAutomatically = false;
  Color backgroundColor = const Color.fromARGB(255, 7, 7, 7);
  static const Color defaultBackgroundColor = Color.fromARGB(255, 7, 7, 7);

  RankingBar({
    super.key,
    required this.widgetWidth,
    this.widgetHeight = 450,
    this.fontSizeMultiplier = 1.0,
    this.titleFontSizeMultiplier = 1.0,
    this.backgroundColor = defaultBackgroundColor,
    this.buttonWidth = 100.0,
    this.buttonHeight = 20.0,
    this.expWidth = 200.0,
    this.scaleAutomatically = false,
  });

  @override
  State<RankingBar> createState() => RankingBarState();
}

class RankingBarState extends State<RankingBar> {
  static final FirebaseService _firebaseService = FirebaseService();
  static List<RankingBarState> instances = [];
  bool showGlobal = false;

  static List players = [
    Player("Steve Blonder", 100, Icons.person_2),
    Player("Holand Frager", 49, Icons.person_3),
    Player("Honeyblocker", 25, Icons.person_4),
    Player("Hendrik", 4269, Icons.baby_changing_station),
    Player("Doge", 42, Icons.pets),
    Player("CryptoBro", 69, Icons.access_alarm),
    Player("Vegane Ravioli", 13, Icons.food_bank),
    Player("Le Deivel", 666, Icons.bloodtype),
    Player("Page not found", 404, Icons.hourglass_empty),
    Player("Spongeyboy", 24, Icons.star),
  ];

  static List<Player> onlinePlayer = [];

  static Player player = Player("Du", 1, Icons.person_pin);

  void refreshPlayerInfo(int level, {bool callSetState = true}) {
    player.level = level;
    player.title = player.titleFromLevel();
    player.userID = PlayerData.userID;
    if (PlayerData.playerName != null && PlayerData.playerName != "") {
      player.name = "${PlayerData.playerName} (Du)";
    } else {
      player.name = "Du";
    }
    if (callSetState && mounted) {
      setState(() {});
    }
  }

  Color colorByPlace(int place) {
    switch (place) {
      case 1:
        return const Color.fromARGB(255, 221, 223, 118);
      case 2:
        return const Color.fromARGB(255, 221, 207, 207);
      case 3:
        return const Color.fromARGB(255, 178, 84, 84);
      default:
        return Colors.grey;
    }
  }

  @override
  void initState() {
    instances.add(this);
    super.initState();
  }

  @override
  void dispose() {
    instances.remove(this);
    super.dispose();
  }

  static void refreshOnlinePlayer() async {
    //List<Player> dataList = await _firebaseService.getOtherPlayers();
    List<Player> dataList = await _firebaseService.getOtherPlayersSupabase();
    onlinePlayer = dataList;
    Player? toRemove;
    for (int i = 0; i < onlinePlayer.length; i++) {
      if (onlinePlayer[i].userID == PlayerData.userID) {
        toRemove = onlinePlayer[i];
      }
    }
    if (toRemove != null) {
      onlinePlayer.remove(toRemove);
    }
    onlinePlayer.add(player);
    onlinePlayer.sort((a, b) => b.level.compareTo(a.level));
  }

  @override
  Widget build(BuildContext context) {
    if (!players.contains(player)) {
      players.add(player);
      player.isPlayer = true;
    }
    players.sort((a, b) => b.level.compareTo(a.level));
    return SizedBox(
      width: widget.scaleAutomatically
          ? 150 + 150 * MediaQuery.of(context).size.width / 1920
          : null,
      child: FittedBox(
        child: SizedBox(
          height: widget.widgetHeight,
          width: widget.widgetWidth,
          child: Column(
            children: [
              PlayerLevel(
                expWidth: widget.expWidth,
              ),
              Container(
                height: 10,
              ),
              Text(
                player.title,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              Container(
                // Spacer
                height: 25,
              ),
              Row(
                // Buttons
                mainAxisAlignment: MainAxisAlignment.center,
                //Buttons haben keine Funktion, weil es "Global" sowieso nicht gibt, daher nur Friends angezeigt werden müssen
                children: [
                  TextButton(
                    onPressed: () => {
                      setState(
                        () => showGlobal = false,
                      )
                    },
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                      foregroundColor: showGlobal
                          ? const Color.fromARGB(255, 69, 69, 72)
                          : Colors.white,
                      backgroundColor: MemeDarkGrey,
                      fixedSize: Size(widget.buttonWidth, widget.buttonHeight),
                    ),
                    child: const Text("NPC"),
                  ),
                  Container(
                    width: 5,
                  ),
                  TextButton(
                    onPressed: () {
                      showGlobal = true;
                      refreshOnlinePlayer();
                      Future.delayed(const Duration(milliseconds: 300),
                          () => setState(() {}));
                    },
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                      foregroundColor: showGlobal
                          ? Colors.white
                          : const Color.fromARGB(255, 69, 69, 72),
                      backgroundColor: const Color.fromARGB(255, 29, 28, 31),
                      fixedSize: Size(widget.buttonWidth, widget.buttonHeight),
                    ),
                    child: const Text("GLOBAL"),
                  ),
                ],
              ),
              Container(
                // Spacer
                height: 10,
              ),
              SizedBox(
                height: widget.widgetHeight * (1.8 / 3.0),
                child: ShaderMask(
                  shaderCallback: (Rect bounds) {
                    return LinearGradient(
                      begin: Alignment.center,
                      end: Alignment.bottomCenter,
                      colors: <Color>[
                        const Color.fromARGB(0, 255, 0, 0),
                        widget.backgroundColor,
                      ],
                    ).createShader(bounds);
                  },
                  blendMode: (MediaQuery.of(context).size.width < 800)
                      ? BlendMode.dst
                      : BlendMode.dstOut,
                  child: ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount:
                        showGlobal ? onlinePlayer.length : players.length,
                    itemBuilder: (BuildContext context, int index) {
                      var usedList = [];
                      if (showGlobal) {
                        usedList = onlinePlayer;
                      } else {
                        usedList = players;
                      }
                      return SizedBox(
                        height: 80 * widget.fontSizeMultiplier,
                        //color: Colors.amber,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "${index + 1}",
                              style: TextStyle(
                                fontSize: (25 * widget.fontSizeMultiplier),
                                fontWeight: FontWeight.bold,
                                color: colorByPlace(index + 1),
                              ),
                            ),
                            Icon(
                              usedList[index].image,
                              size: 60 * widget.fontSizeMultiplier,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: widget.widgetWidth / 2,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${usedList[index].name}",
                                    style: TextStyle(
                                        color: usedList[index].isPlayer == true
                                            ? const Color.fromARGB(
                                                255, 221, 255, 0)
                                            : Colors.white,
                                        fontSize:
                                            15 * widget.fontSizeMultiplier,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "${usedList[index].title} // LVL ${usedList[index].level}",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15 * widget.fontSizeMultiplier,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Player {
  String name;
  String title = "Rookie";
  int level;
  IconData image;
  bool isPlayer = false;
  String userID = "";

  Player(this.name, this.level, [this.image = Icons.person_off_outlined]) {
    title = titleFromLevel();
  }

  String titleFromLevel() {
    switch (level) {
      case < 10:
        return "Rookie";
      case < 20:
        return "Apprentice";
      case < 30:
        return "Knight";
      case < 40:
        return "Greater Knight";
      case < 50:
        return "Merchant";
      case < 60:
        return "Greater Merchant";
      case < 70:
        return "Meme-Junky";
      case < 85:
        return "Extraordinary Meme-Junky";
      case < 100:
        return "Aristocrat";
      case < 150:
        return "Meme King";
      case < 500:
        return "Meme Lord";
      case < 1000:
        return "Unstoppable";
      case < 5000:
        return "Unbelievable";
      case < 15000:
        return "Rising God";
      case < 50000:
        return "Meme God";
      case >= 50000:
        return "Monstrousity";
      default:
        return "Rookie";
    }
  }
}
