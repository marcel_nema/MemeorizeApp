// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class QuizDataAdapter extends TypeAdapter<QuizData> {
  @override
  final int typeId = 0;

  @override
  QuizData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return QuizData()
      ..image = fields[0] as String
      ..title = fields[1] as String;
  }

  @override
  void write(BinaryWriter writer, QuizData obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.image)
      ..writeByte(1)
      ..write(obj.title);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuizDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
