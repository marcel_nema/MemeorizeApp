/*

Autor: Felix

Dies ist das Widget, das für Funktionen benutzt wird,
die eine Passwort-Eingabe erfordern, weil man ansonsten
Memes löscht o.Ä. die dann bei allen nicht mehr vorhanden
sind und dieses Funktion dem normalen Nutzer nicht zur
Verfügung stehen soll.

*/

import 'dart:convert';
import 'package:flutter/material.dart';
import 'style.dart';

class AdminPasswordField extends StatelessWidget {
  static String adminPassword = "WirdAutomatischErsetzt";
  static int lastTry = 0;
  static int timeBetweenTries = 15;
  final double constrainedWidth;

  const AdminPasswordField({
    super.key,
    required TextEditingController passwordController,
    this.constrainedWidth = double.infinity,
  }) : _passwordController = passwordController;

  final TextEditingController _passwordController;

  @override
  Widget build(BuildContext context) {
    adminPassword = buildPassword();
    if (adminPassword == "") {
      adminPassword = "HätteErsetztWerdenSollen";
    }
    return Container(
      constraints: BoxConstraints(maxWidth: constrainedWidth),
      child: TextFormField(
        obscureText: true,
        controller: _passwordController,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Admin Password",
          fillColor: Colors.black,
          filled: true,
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
        ),
        style: const TextStyle(color: Colors.white),
      ),
    );
  }

  static bool isAllowedToCheckPassword(BuildContext context) {
    int result = DateTime.now().millisecondsSinceEpoch ~/
            Duration.millisecondsPerSecond -
        lastTry;
    if (result > timeBetweenTries) {
      return true;
    } else {
      showWaitTimeDialog(context);
      return false;
    }
  }

  static void resetPasswordTime() {
    lastTry =
        DateTime.now().millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
  }

  static void showPasswordFailedDialog(BuildContext context) {
    resetPasswordTime();
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: MemeDarkGrey,
          title: const Center(
            child: Text(
              "Wrong Password",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: [
            Center(
              child: TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("OK"),
              ),
            )
          ],
        );
      },
    );
  }

  static void showWaitTimeDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: MemeDarkGrey,
          title: Center(
            child: Text(
              "Du musst noch ${timeBetweenTries - (DateTime.now().millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond - lastTry)} Sekunden warten. Versuche es dann erneut.",
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: [
            Center(
              child: TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("OK"),
              ),
            )
          ],
        );
      },
    );
  }

  /*
  Einfach nur dafür da, dass das Passwort nicht als String
  im generierten .js File steht, wo es jeder direkt rauslesen kann,
  sondern es zumindest etwas komplizierter wird (aber nicht unmöglich)
  */
  String buildPassword() {
    List<int> bytes = [
      75,
      114,
      97,
      115,
      115,
      83,
      116,
      97,
      114,
      107,
      101,
      115,
      80,
      97,
      115,
      115,
      119,
      111,
      114,
      116
    ];
    String result = utf8.decode(bytes);
    return result;
  }
}

class UserDataEncoder {
  static String encodeUID(String uid) {
    List<int> bytified = utf8.encode(uid);
    String output = "";
    for (int i = 0; i < bytified.length; i++) {
      bytified[i] = bytified[i] * 3 + (11 + 7 * i);
      output += correspondingChar(bytified[i]);
    }
    return output;
  }

  //Decode nicht benutzen!
  static String decodeUID(String encoded) {
    List<int> bytified = ascii.encode(encoded);
    for (int i = 0; i < bytified.length; i++) {
      bytified[i] = (bytified[i] - (11 + 7 * i)) ~/ 3;
    }
    String output = utf8.decode(bytified);
    return output;
  }

  static String correspondingChar(int inputString) {
    inputString = inputString % 10;
    switch (inputString) {
      case 0:
        return "#";
      case 1:
        return "!";
      case 2:
        return "%";
      case 3:
        return "?";
      case 4:
        return "+";
      case 5:
        return "-";
      case 6:
        return "~";
      case 7:
        return "0";
      case 8:
        return "&";
      case 9:
        return "#";
      case 10:
        return "§";
    }
    return "_";
  }
}
