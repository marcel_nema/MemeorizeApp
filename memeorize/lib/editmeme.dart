/*
@Autorin: Charlotte

Anzeige der Seite zum Bearbeiten eines Memes
Die TextFields sind vorausgefüllt mit den Daten des Memes, das bearbeitet werden soll.
Titel und Info können bearbeitet werden. Das Bild kann durch ein anderes ersetzt werden.
Wird ein Feld nicht bearbeitet, so wird der alte Wert übernommen.
Klickt man auf "Update Meme", so wird das Meme in der Datenbank aktualisiert.

@Mitwirkender: Hendrik
An der Umsetzung mit Firebase
---------------------------------------------------------------------------------*/

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hovering/hovering.dart';
import 'package:image_picker_web/image_picker_web.dart';
//import 'package:memeorize/style.dart';
import 'firebaseservice.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:html' as html;

class EditMeme extends StatefulWidget {
  final DocumentSnapshot quizData;

  const EditMeme({super.key, required this.quizData});

  @override
  _EditMemeState createState() => _EditMemeState();
}

class _EditMemeState extends State<EditMeme> {
  late TextEditingController _titleController;
  late TextEditingController _infoController;
  final FirebaseService _firebaseService = FirebaseService();
  html.File? _image;
  String? _currentImageUrl;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController(text: widget.quizData['title']);
    _infoController =
        TextEditingController(text: widget.quizData['info'] ?? '');
    _currentImageUrl = widget.quizData['imageUrl'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 14, 18, 20),
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 14, 18, 20),
        title: const Text('Edit Meme', style: TextStyle(color: Colors.white)),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width / 3,
                height: MediaQuery.of(context).size.height / 2,
                //child: Expanded(
                child: HoverWidget(
                  hoverChild: buildHoverCard2(),
                  onHover: (PointerEnterEvent event) {},
                  child: buildMemeCard(),
                ),
                //),
              ),
            ),
            Container(
              color: Colors.black,
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                // Eingabefeld für den Titel
                // Vorausgefüllt mit dem alten Titel
                cursorColor: Colors.white,
                controller: _titleController,
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                  labelText: 'Title',
                  labelStyle:
                      TextStyle(color: Color.fromARGB(84, 255, 255, 255)),
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(8.0),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.black,
                ),
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height * 0.3,
                  ),
                  child: SingleChildScrollView(
                    child: TextField(
                      // Eingabefeld für die Info
                      // Vorausgefüllt mit der alten Info
                      cursorColor: Colors.white,
                      controller: _infoController,
                      style: const TextStyle(color: Colors.white),
                      maxLines: null, // Allows the text field to expand
                      decoration: const InputDecoration(
                        labelText: 'Info',
                        labelStyle:
                            TextStyle(color: Color.fromARGB(84, 255, 255, 255)),
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(8.0),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            
            Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
              child: SizedBox(
                width: MediaQuery.of(context).size.width / 7,
                height: MediaQuery.of(context).size.height / 20,
                child: GestureDetector(
                  // Button zum Aktualisieren des Memes
                  onTap: () {
                    if (_image != null) {
                      // Updatet das Meme mit neuem Bild
                      // Siehe firebaseservice.dart
                      _firebaseService.updateMeme(
                        widget.quizData.id,
                        _titleController.text,
                        _infoController.text,
                        _image!,
                      );
                    } else {
                      // Updatet das Meme ohne das Bild zu ändern
                      // Siehe firebaseservice.dart
                      _firebaseService.updateMemeWithoutImage(
                        widget.quizData.id,
                        _titleController.text,
                        _infoController.text,
                      );
                    }
                    Navigator.pop(context);
                    showDialog(
                      // Zeigt Dialog an, dass das Meme aktualisiert wurde
                      barrierColor: Colors.white.withOpacity(0),
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        Future.delayed(const Duration(seconds: 2), () {
                          Navigator.of(context).pop(true); // Schließt Dialog
                        });
                        return const AlertDialog(
                          backgroundColor: Color.fromARGB(255, 221, 255, 0),
                          alignment: AlignmentDirectional.bottomEnd,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0))),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Meme updated',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.check,
                                  color: Colors.black,
                                  size: 25,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  child: Container(
                    color: const Color.fromARGB(255, 219, 255, 0),
                    alignment: Alignment.center,
                    child: const Center( // Center-Widget hinzugefügt
                      child: AutoSizeText(
                        'Update Meme',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                        maxLines: 1,
                      )),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Funktion zum Auswählen eines Bildes
  Future<void> _pickImage() async {
    html.File? imageFile = await ImagePickerWeb.getImageAsFile();

    if (imageFile != null) {
      setState(() {
        _image = imageFile;
      });
      _uploadImage(imageFile);
    }
  }

  // Widget zum Anzeigen des Memes
  // Wird ein Bild ausgewählt, so wird dieses angezeigt
  Widget buildMemeCard() {
    String imageUrl = _currentImageUrl ?? widget.quizData['imageUrl'];
    return Card(
      child: Column(
        children: [
          Expanded(
            child: Image.network(
              imageUrl,
              fit: BoxFit.cover, // Bild wird skaliert, sodass es passt
            ),
          ),
        ],
      ),
    );
  }

  // Wenn über das Bild gehovert wird, so wird es dunkler und ein Icon und Text erscheinen,
  // die zum Auswählen eines Bildes auffordern
  Widget buildHoverCard2() {
    String imageUrl = _currentImageUrl ?? widget.quizData['imageUrl'];
    return InkWell(
      onTap: _pickImage,
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.network(
              imageUrl,
              fit: BoxFit.cover,
              color: Colors.black.withOpacity(0.4),
              colorBlendMode: BlendMode.darken,
            ),
            Icon(
              Icons.camera_alt,
              size: MediaQuery.of(context).size.width / 30,
              color: Colors.white,
            ),
            Text(
              'Upload an image',
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width / 60,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Funktion zum Hochladen eines Bildes
  void _uploadImage(html.File imageFile) async {
    String imageUrl = await _firebaseService.uploadImage(imageFile);
    if (imageUrl.isNotEmpty) {
      await _firebaseService.updateMemeImage(widget.quizData.id, imageUrl);
      setState(() {
        _currentImageUrl = imageUrl;
      });
    }
  }
}
