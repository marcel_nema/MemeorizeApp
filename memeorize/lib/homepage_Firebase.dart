import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:memeorize/Sounds.dart';
import 'quizData_Firebase.dart';
import 'dart:async';
import 'style.dart';
import 'player_level.dart';
import 'components/meme_quiz_container.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:provider/provider.dart';
import '/main.dart'; 
import 'dart:math';



/*--------------------------------------------------------------------------

Angefangen von: 

Komplett neu überarbeitet von Marcel, zum Zweck des Responsive Designs

Quiz überarbeitet von Hendrik, um alle Firebase Funktionaitäten zu implementieren,
und responsiveness Optimierungen

--------------------------------------------------------------------------*/

class Homepage_Firebase extends StatefulWidget {
  const Homepage_Firebase({super.key});

  @override
  _Homepage_FirebaseStateState createState() => _Homepage_FirebaseStateState();
}

class _Homepage_FirebaseStateState extends State<Homepage_Firebase> {
  final QuizDataFirebase quizData = QuizDataFirebase();
  late Future<Map<String, bool>> questionAndAnswersFuture;
  Map<String, bool> questionAndAnswers = {};
  List<MapEntry<String, bool>> preLoadedShuffledAnswers = [];
  List<MapEntry<String, bool>> shuffledAnswers = [];
  String imageUrl = '';
  bool isAnswerSelected = false;
  bool isAnswerCorrect = false;
  String selectedAnswer = '';
  String correctAnswer = '';
  List<String> notTheNextTenQuestions = [];
  AudioPlayer audioPlayer = AudioPlayer();
  AudioPlayer audioPlayerTimer = AudioPlayer();
  int wrongAnswersInARow = 0;

  Duration soundDuration = const Duration();
  bool audioPlayerMuted = false;

  Timer? myTimer;
  double remainingTime = 15;
  bool bonusResetTimerHasRun = false;
  bool lastSecondsSoundStarted = false;



  // Ranking-System Variablen
  int consecRightAnswers = 0;
  int currentLevel = 0;
  int maxLevel = 6;
  double textScale = 1.0;
  double maxTimerLimit = 20;

  /// Toggle AudioPlayerMuted
  void toggleAudioPlayerMuted() {
    setState(() {
      audioPlayerMuted = !audioPlayerMuted;

      if (audioPlayerMuted == true) {
        audioPlayer.setVolume(0);
        audioPlayerTimer.setVolume(0);
      } else {
        audioPlayer.setVolume(1);
        audioPlayerTimer.setVolume(1);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    
    questionAndAnswersFuture = loadQuizData(mix: true);
  }

@override
void dispose() {
  myTimer?.cancel();
  super.dispose();
}

  /// Lädt 4 Memes in eine Map mit Titel, True und False als Value
  Future<Map<String, bool>> loadQuizData({bool mix = true}) async {
    try {
      var randomMemeData = await quizData.randomeMemeTitleAndUrl();
      while (notTheNextTenQuestions.contains(randomMemeData['title'])) {
        randomMemeData = await quizData.randomeMemeTitleAndUrl();
      }

      questionAndAnswers = await quizData
          .generateQuestionAndAnswers(randomMemeData['title'] ?? '');
      correctAnswer = randomMemeData['title'] ?? '';
      setState(() {
        imageUrl = randomMemeData['imageUrl'] ?? '';

        notTheNextTenQuestions.add(randomMemeData['title'] ?? '');
        if (notTheNextTenQuestions.length > 10) {
          notTheNextTenQuestions.removeAt(0);
        }
        //print("notTheNextTenQuestions: $notTheNextTenQuestions");
      });
      preLoadedShuffledAnswers = questionAndAnswers.entries.toList();
      if (mix) {
        preLoadedShuffledAnswers.shuffle();
      }
    } catch (e) {
      print("Fehler beim Laden der Quizdaten: $e");
    }

    return questionAndAnswers;
  }



void resetMultiplier() {
  // Setze hier deinen Multiplikator zurück
  currentLevel = 0;
  consecRightAnswers = 0;
  // Aktualisiere den State, wenn nötig
  setState(() {});
}



void startTimer() {


 
  myTimer = Timer.periodic(Duration(seconds: 1), (timer) {
    if (remainingTime == 0) {
      timer.cancel();
    }


    if(remainingTime <= 5 && lastSecondsSoundStarted == false ) {
      lastSecondsSoundStarted = true;
      last3SecondsSound();
      print("Last 3 Seconds Sound started");
      
    }





    if (remainingTime > 0) {
      setState(() {
        remainingTime--;
      });
    } else {
      timer.cancel();
      resetMultiplier(); // Funktion, die aufgerufen wird, wenn der Timer abläuft
      
    }
  });
}


void stopAndResetTimer() {
  lastSecondsSoundStarted = false;
  myTimer?.cancel();
  myTimer = null;
}

void last3SecondsSound() async {
  try {
    // asset 3-s-left.mp3
    WidgetsBinding.instance.addPostFrameCallback((_) { 
      if(!kIsWeb) {
        audioPlayerTimer.play(UrlSource("assets/sounds/3-s-left.mp3"));
      } else {
        audioPlayerTimer.play(UrlSource("assets/assets/sounds/3-s-left.mp3"));
      }
    });
    
  } catch (e) {
    print("Fehler beim Abspielen des Sounds: $e");
  }
}

  /// Prüft die Antwort und führt entsprechende Aktionen aus
  void _checkAnswer(
      String answer, bool isCorrect, ThemeProvider themeProvider) {
    int timerDuration = 800;
    stopAndResetTimer();
    audioPlayerTimer.stop();

    setState(() {
      selectedAnswer = answer;
      isAnswerSelected = true;
      isAnswerCorrect = isCorrect;

      // RICHTIGE ANTWORT
      if (isAnswerCorrect) {
        try {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            audioPlayer.play(UrlSource(SoundEffects().randomCorrectSound()));
          });
          // audioPlayer.onDurationChanged.listen((Duration d) {
          // Die Dauer in Millisekunden ausgeben
          // print('Duration: ${d.inMilliseconds}');
          //timerDuration = d.inMilliseconds;
          //});
        } catch (e) {
          print("Fehler beim Abspielen des Sounds: $e");
        }

        consecRightAnswers++;
        wrongAnswersInARow = 0;
        if (consecRightAnswers >= 5) {
          themeProvider.setComicSansMode(false);
        }
        PlayerData.addExperience(
          1 + getMultiplierByConsec(consecRightAnswers),
        );
        currentLevel =
            consecRightAnswers - getLastMultiplierMilestone(consecRightAnswers);
        //print("EXP: ${PlayerData.experience} $consecRightAnswers");
        timerDuration = 600;
      }
      // FALSCHE ANTWORT
      else {
        try {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            audioPlayer.play(UrlSource(SoundEffects().randomFailSound()));
          });

          //audioPlayer.onDurationChanged.listen((Duration d) {
          // Die Dauer in Millisekunden ausgeben
          // int soundMilliseconds = d.inMilliseconds;
          // timerDuration = soundMilliseconds;
          // print("SoundDuration: $soundMilliseconds");
          // });
        } catch (e) {
          print("Fehler beim Abspielen des Falsch Sounds: $e");
        }
        consecRightAnswers = 0;
        //print("EXP: ${PlayerData.experience} $consecRightAnswers");
        wrongAnswersInARow++;

        // Prüfen, ob 3 falsche Antworten hintereinander gegeben wurden,
        // dann ComicSansMode aktivieren
        if (wrongAnswersInARow >= 3) {
          themeProvider.setComicSansMode(true); // ComicSansMode aktivieren
        }
        timerDuration = 1200;
      }
      maxLevel = getNextMultiplierMilestone(consecRightAnswers) -
          getLastMultiplierMilestone(consecRightAnswers);
      currentLevel =
          consecRightAnswers - getLastMultiplierMilestone(consecRightAnswers);
      textScale = 1.0 + getMultiplierByConsec(consecRightAnswers) * 0.01;
    });
    print("TimerDuration: $timerDuration");
    Timer(Duration(milliseconds: timerDuration), () {
      setState(() {
        print("Timer");
        loadQuizData();
        isAnswerSelected = false;
        shuffledAnswers = preLoadedShuffledAnswers;
        selectedAnswer = '';
        bonusResetTimerHasRun = true;
        remainingTime = 15 / (log(getMultiplierByConsec(consecRightAnswers)+1));
        maxTimerLimit = 15 / (log(getMultiplierByConsec(consecRightAnswers)+1));
          
          startTimer();
        
      });
    });
  }

//_______________________________________________________
// Hier steht alles zum Ranking-System

  /*Autor: Felix
  Gibt den aktuellen Multiplikator anhand
  von in Folge richtig erratenen Memes zurück.

  Da diese Funktion von allen anderen für die
  Berechnungen benutzt wird, können hier die
  Schwellwerte verändert werden, um anzupassen,
  wann welcher Multiplikator erreicht wird, oder 
  es können neue Multiplikatoren hinzugefügt 
  werden.
  */
  double getMultiplierByConsec(int consec) {
    switch (consec) {
      case 0:
        return 1;
      case 1:
        return 2;
      case >= 2 && < 6:
        return 3;
      case >= 6 && < 11:
        return 5;
      case >= 11 && < 19:
        return 7;
      case >= 19 && < 29:
        return 9;
      case >= 29 && < 42:
        return 12;
      case >= 42 && < 69:
        return 15;
      case >= 69 && < 111:
        return 18;
      case >= 111 && < 167:
        return 22;
      case >= 167 && < 213:
        return 26;
      case >= 213 && < 345:
        return 30;
      case >= 345 && < 666:
        return 35;
      case >= 666:
        return 40;
    }
    return 0;
  }

  /*Autor: Felix
  Errechnet und gibt den nächsten Multiplikator
  zurück, der erreicht wird, wenn mehr Memes in Folge
  richtig erraten werden.
  */
  double getNextMultiplier(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    double nextMultiplier = currentMultiplier;
    for (int i = 0; i < 350; i++) {
      if (getMultiplierByConsec(currentConsec + i) > currentMultiplier) {
        nextMultiplier = getMultiplierByConsec(currentConsec + i);
        return nextMultiplier;
      }
    }
    return currentMultiplier;
  }

  /*Autor: Felix 
  Errechnet und gibt den nächsten Multiplikator 
  zurück, der erreicht wird, wenn mehr Memes in Folge 
  richtig erraten werden. 
  */
  double getLastMultiplier(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    double lastMultiplier = currentMultiplier;
    for (int i = 0; i < 350; i++) {
      if (getMultiplierByConsec(currentConsec - i) < currentMultiplier) {
        lastMultiplier = getMultiplierByConsec(currentConsec - i);
        return lastMultiplier;
      }
    }
    return currentMultiplier;
  }

  /*Autor: Felix
  Errechnet und gibt die Menge der nötigen, in Folge
  richtig erratenen Memes zurück, die benötigt werden,
  um zum nächsten Multiplikator aufzusteigen, anhand
  der aktuell in Folge richtig erratenen Memes.
  */
  int getNextMultiplierMilestone(int currentConsec) {
    int nextMilestone = currentConsec;
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    for (int i = 0; i < 350; i++) {
      if (getMultiplierByConsec(currentConsec + i) > currentMultiplier) {
        nextMilestone = currentConsec + i;
        return nextMilestone;
      }
    }
    return 999999;
  }

  /*Autor: Felix
  Errechnet und gibt die Menge der in Folge richtig
  erratenen Memes zurück, die nötig gewesen sind, um
  den vorherigen Multiplikator zu erreichen, anhand der
  aktuell in Folge richtig erratenen Memes.
  */
  int getLastMultiplierMilestone(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    for (int i = 0; i < 350; i++) {
      if (getMultiplierByConsec(currentConsec - i) < currentMultiplier) {
        return currentConsec - i + 1;
      }
    }
    return 0;
  }

// Ranking-System ENDE
//_______________________________________________________________________________

  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Positioned(
                right: 10,
                top: 10,
                child: IconButton(
                  // Mute Button
                  color: Colors.white,
                  onPressed: () {
                    setState(() {
                      toggleAudioPlayerMuted();
                    });
                  },
                  icon: Icon(
                    audioPlayerMuted == true
                        ? Icons.volume_off
                        : Icons.volume_up,
                    color: Colors.white,
                  ),
                )),
            Container(
              constraints:
                  BoxConstraints(minHeight: MediaQuery.of(context).size.height),
              child: Container(
                decoration: BoxDecoration(
                  // Wenn ComicSansMode aktiviert ist, dann verwende den ComicSansGradient
                  // ansonsten verwende color MemeBlackGreen
                  gradient: themeProvider.isComicSansMode == true
                      ? ComicSansGradient
                      : null,

                  color: themeProvider.isComicSansMode == true
                      ? null
                      : MemeBlackGreen,
                ),
                child: Center(
                  child: Container(
                    child: FutureBuilder<Map<String, bool>>(
                      future: questionAndAnswersFuture,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                              child: CircularProgressIndicator());
                        } else if (snapshot.hasError) {
                          return const Center(
                              child: Text('Ein Fehler ist aufgetreten!'));
                        } else if (!snapshot.hasData ||
                            snapshot.data!.isEmpty) {
                          return const Center(
                              child: Text('Keine Daten verfügbar.'));
                        } else {
                          return Padding(
                            padding: EdgeInsets.only(
                              left: 20.0, // Links konstant 20
                              right: 20.0, // Rechts konstant 20
                              bottom: 20.0, // Unten konstant 20
                              top: MediaQuery.of(context).size.height < 800 ||
                                      MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                  ? 20.0
                                  : MediaQuery.of(context).size.height / 6,
                              // Oben 1/6 der Bildschirmhöhe, außer die Bildschirmhöhe ist kleiner als 1000 oder im Hochformat, dann 20
                            ),
                            child: Column(
                              children: [
                                Container(
                                  height: 450,
                                  constraints: const BoxConstraints(
                                    maxWidth: 500,
                                    minHeight: 100,
                                  ),
                                  child: MemeQuizContainer(
                                    imageUrl: imageUrl,
                                    isAnswerSelected: isAnswerSelected,
                                    isAnswerCorrect: isAnswerCorrect,
                                    correctAnswers: consecRightAnswers,
                                    maxLevel: maxLevel,
                                    currentLevel: currentLevel,
                                    textScale: textScale,
                                    activeMultiplier: getMultiplierByConsec(
                                        consecRightAnswers),
                                    beforeMultiplier:
                                        getLastMultiplier(consecRightAnswers),
                                    nextMultiplier: getNextMultiplier(consecRightAnswers),
                                    neededForBonus:
                                        getNextMultiplierMilestone(consecRightAnswers),
                                        wrongAnswersInARow: wrongAnswersInARow,
                                        remainingTime: remainingTime,
                                        timerHasRun: bonusResetTimerHasRun,
                                        maxTimerLimit: maxTimerLimit,

                                  ),
                                ),
                                Container(
                                  constraints: const BoxConstraints(
                                    maxHeight: 400,
                                    maxWidth: 1000,
                                    minHeight: 100,
                                  ),
                                  child: GridView.count(
                                    scrollDirection: Axis.vertical,
                                    crossAxisSpacing: 10.0,
                                    mainAxisSpacing: 10.0,
                                    childAspectRatio:
                                        // bis 1300 Breite feste Größe
                                        MediaQuery.of(context).size.width > 1300
                                            ? 7
                                            // ab 600 Breite dynamische Größe
                                            : MediaQuery.of(context)
                                                        .size
                                                        .width >
                                                    600
                                                ? ((MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        1.2) /
                                                    200)
                                                // Hochformat wieder feste Größe
                                                : 8,
                                    shrinkWrap: true,

                                    crossAxisCount:
                                        // Mobil eine Spalte, Tablet etc zwei Spalten
                                        MediaQuery.of(context).size.width < 600
                                            ? 1
                                            : 2,
                                    children:
                                        preLoadedShuffledAnswers.map((entry) {
                                      String answer = entry.key; // Key: Titel
                                      bool isCorrect =
                                          entry.value; // Value: true/false
                                      bool isSelected =
                                          answer == selectedAnswer;
                                      return Container(
                                          child: ElevatedButton(
                                        onPressed: isAnswerSelected
                                            ? null
                                            : () => _checkAnswer(answer,
                                                isCorrect, themeProvider),
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty
                                              .resolveWith<OutlinedBorder>(
                                            (states) {
                                              return RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(3.0),
                                              );
                                            },
                                          ),
                                          foregroundColor: MaterialStateProperty
                                              .resolveWith<Color>(
                                            (states) {
                                              return Colors.white;
                                            },
                                          ),
                                          backgroundColor: MaterialStateProperty
                                              .resolveWith<Color>(
                                            (states) {
                                              if (isSelected) {
                                                return isCorrect
                                                    ? MemeGreen
                                                    : MemeRed;
                                              } else if (states.contains(
                                                  MaterialState.hovered)) {
                                                return MemeBlue; // Hover-Farbe
                                              } else if (isAnswerSelected &&
                                                  isCorrect) {
                                                return Colors
                                                    .lightGreen; // richtige Antwort wird grün ...
                                              } else if (isAnswerSelected) {
                                                return Colors
                                                    .grey; // Andere Buttons werden grau, wenn eine Antwort ausgewählt wurde
                                              }
                                              return MemeDarkGrey; // Standardfarbe
                                            },
                                          ),
                                          // Textfarbe on hover MemeGrey, ansonsten we
                                        ),
                                        child: FittedBox(
                                            child: Text(
                                          answer,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium
                                              ?.copyWith(
                                                fontSize: 20,
                                              ),
                                        )),
                                      ));
                                    }).toList(), // Konvertiere das Ergebnis von map() in eine Liste
                                  ),
                                )
                              ],
                            ),
                          );
                        }
                      },
                    ),
                                      ),
                                      ),
                                    ),
                                  
                                ),


          Positioned( 
                    right: 10, 
                    top: 10, 
                    child: Column(
                      children: [
                        // Timer
                        Text(
                          remainingTime.toStringAsFixed(0),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                          ),
                        ),
                        IconButton( 
                          // Mute Button 
                          color: Colors.white, 
                          onPressed: () { 
                            setState(() { 
                              toggleAudioPlayerMuted(); 
                            }); 
                          }, 
                          icon: Icon( 
                             
                            audioPlayerMuted == true 
                                ? Icons.volume_off 
                                : Icons.volume_up, 
                         
                            color: Colors.white, 
                          ),),
                      ],
                    )), ],
        ),
      ),
    );
  }
}
