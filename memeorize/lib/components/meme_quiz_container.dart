/*

Autor dieser Datei:
Hendrik

Diese Datei dient zur Anzeige des Quiz-Containers
bestehend aus Bild, EXP-Bar und Feedback
ist möglichst Responsive und passt sich an die
Größe des Bildschirms an
*/

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../style.dart';
import 'package:linear_progress_bar/linear_progress_bar.dart';


/// Widget für die Anzeige des Quiz-Containers
/// bestehend aus Bild, EXP-Bar und Feedback
final class MemeQuizContainer extends StatelessWidget {
  final String imageUrl;
  final bool isAnswerSelected;
  final bool isAnswerCorrect;
  final int correctAnswers;
  final int maxLevel;
  final int currentLevel;
  final double textScale;
  final double activeMultiplier;
  final double beforeMultiplier;
  final double nextMultiplier;
  final int neededForBonus;
  final int wrongAnswersInARow;
  final double remainingTime;
  bool timerExpired = false;
  final timerHasRun;
  double maxTimerLimit;


  // Konstruktor, um Parameter zu initialisieren
   MemeQuizContainer({
    super.key,
    required this.imageUrl,
    required this.isAnswerSelected,
    required this.isAnswerCorrect,
    required this.correctAnswers,
    required this.maxLevel,
    required this.currentLevel,
    required this.textScale,
    required this.activeMultiplier,
    required this.beforeMultiplier,
    required this.nextMultiplier,
    required this.neededForBonus,
    required this.wrongAnswersInARow,
    required this.remainingTime,
    required this.timerHasRun,
    required this.maxTimerLimit,

  });


String memeStreakTitle(int index) {
  List<String> goodTitles = [
"Good",
"Great",
"Excellent",
"Impressive",
"Wonderful",
"Superb",
"Fantastic",
"Marvellous",
"Splendid",
"Magnificent",
"Outstanding",
"Exceptional",
"Stupendous",
"Phenomenal",
"Remarkable",
"Astonishing",
"Amazing",
"Breathtaking",
"Incredible",
"Sensational",
"Awesome",
"Spectacular",
"Sublime",
"Prodigious",
"Stunning",
"Extraordinary",
"Astounding",
"Miraculous",
"Unbelievable",
"Absolutely godlike",
  ];

  List<String> badTitles = [
"Somewhat bad",
"Rather poor",
"Fairly terrible",
"Quite awful",
"Pretty dismal",
"Decidedly dreadful",
"Majorly miserable",
"Seriously subpar",
"Truly tragic",
"Horrifically horrible",
"Disastrously disastrous",
"Catastrophically catastrophic",
"Appallingly appalling",
"Shockingly shocking",
"Terrifyingly terrible",
"Frighteningly frightful",
"Alarmingly alarming",
"Ghastly ghastly",
"Gruesomely gruesome",
"Hideously hideous",
"Abysmally abysmal",
"Dismally dismal",
"Nightmarishly nightmarish",
"Unspeakably unspeakable",
"Unimaginably unimaginable",
"Unbearably unbearable",
"Inexpressibly inexpressible",
"Inconceivably inconceivable",
"Unfathomably unfathomable",
"Absolute annihilation-level bad",
  ];

  if (index <= -1) {
    // Negative Indizes verwenden die schlechten Titel
    int idx = -index - 1; // Umwandlung in einen gültigen Index
    if (idx >= badTitles.length) {
      return badTitles.last;
    }
    return badTitles[idx];
  } else if (index == 0) {
    // Index 0 ist der neutrale Titel
    return "";
  }else {
    // Positive Indizes verwenden die guten Titel
    if (index >= goodTitles.length) {
      return goodTitles.last;
    }
    return goodTitles[index];
  }
}



    
  
 
  @override
  Widget build(BuildContext context) {
    // Erstelle das UI deines Widgets
    



    if (correctAnswers < 1 && timerHasRun)
    {
      timerExpired = true;
    }
    return Scaffold(
      
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.transparent,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            // Bild und Feedback Icon
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [

                    Container(
                      padding: const EdgeInsets.all(20),
                      decoration: const BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                      ),
                      child: ClipRRect(
                        // oben abgerundete Ecken
                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                        child: FittedBox(
                          clipBehavior: Clip.hardEdge,
                          child: Image.network(
                            height: 300,
                            width: 500,
                            imageUrl,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    if (isAnswerSelected) // Hacken Icon zeigen
                      Icon(
                        isAnswerCorrect ? Icons.check_circle : Icons.cancel,
                        color: isAnswerCorrect ? MemeGreen : MemeRed,
                        size: 100,
                      ),
                  ],
                ),
              ],
            ),
            // EXP Bar
            Container(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: 30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "$correctAnswers",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(148, 255, 255, 255),
                              fontSize: 14),
                        ),
                        Text(
                          memeStreakTitle(correctAnswers - wrongAnswersInARow),
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontSize: 14),
                        ),
                        Text(
                          "$neededForBonus",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(146, 255, 255, 255),
                              fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: LinearProgressBar(
                        maxSteps: maxLevel,
                        progressType: LinearProgressBar.progressTypeLinear,
                        currentStep: currentLevel,
                        minHeight: 7,
                        progressColor: const Color.fromRGBO(96, 250, 251, 1),
                        backgroundColor: MemeGrey,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Text(
                          "${beforeMultiplier.toString()}×",
                          style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Color.fromARGB(150, 255, 255, 255),
                              fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right: 10),
                                        child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Icon(timerExpired == true
                                          // Kreuz Icon zeigen
                                          ? Icons.cancel
                                          : Icons.flash_on, 
                                          color: timerExpired == false 
                                          ? MemeBlue
                                          : MemeRed, 
                                          size: 20,),
                                          CircularProgressIndicator(
                                                              value: remainingTime / maxTimerLimit, 
                                                              backgroundColor: Colors.grey,
                                                              valueColor: remainingTime > 3
                                                                ? AlwaysStoppedAnimation<Color>(MemeBlue)
                                                                : AlwaysStoppedAnimation<Color>(MemeRed),
                                                            ),
                                        ],
                                                                          ),
                                      ),
                            AnimatedScale(
                              scale: textScale,
                              duration: const Duration(milliseconds: 300),
                              curve: const MultiplierCurve(),
                              child: Column(
                                children: [
                                  Center(
                                    child: Text(
                                      "${activeMultiplier.toString()}×",
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w700,
                                        height: 1,
                                        fontSize: 30,
                                        color: Color.fromARGB(255, 255, 255, 255),
                                      ),
                                    ),
                                  ),
                                
                                   Text("COMBO",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w900,
                                          color:
                                              Color.fromARGB(255, 255, 255, 255),
                                          fontSize: 10))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Text("${nextMultiplier.toString()}×",
                            style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                color: Color.fromARGB(150, 255, 255, 255),
                                fontSize: 20)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/*Autor: Felix
Eine stark verstärkte Standardkurve, um
die Zahl für die Animation stark zu vergrößern.  
*/
class MultiplierCurve extends Curve {
  const MultiplierCurve({
    this.multiplier = 50,
  });
  final double multiplier;
  final double maxExpansion = 50;

  @override
  double transformInternal(double t) {
    return clampDouble(Curves.easeInOutBack.transformInternal(t) * multiplier,
        -maxExpansion, maxExpansion);
        
  }
  
}
