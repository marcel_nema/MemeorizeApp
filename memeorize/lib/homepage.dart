// Komentare wurden in Homepage Firebase übernommen
// Hier bitte nicht mehr weiterarbeiten  


import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:memeorize/database.dart';
import 'dart:math';
import 'package:linear_progress_bar/linear_progress_bar.dart';
import 'widgets.dart';
import 'player_level.dart';

/*--------------------------------------------------------------------------

Angefangen von: Charlotte

Komplett neu überarbeitet von Marcel, zum Zweck des Responsive Designs

Diese Datei ist veraltet und wird nicht mehr verwendet. Sie wurde durch
homepage_Firebase.dart ersetzt. Es macht Charlotte aber sehr traurig, sie
zu löschen, deshalb bleibt sie hier.

*/ //-----------------------------------------------------------------------

class Homepage extends StatefulWidget {
  final Box<QuizData> database;
  const Homepage({super.key, required this.database});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int currentLevel = 0;
  int maxLevel = 6;
  double textScale = 1.0;
  int consecRightAnswers = 0;
  int selectedAnswerIndex = -1; // Initialize with an invalid index
  bool isAnswerCorrect = false;
  late List<QuizData> quizData = [];
  final random = Random();
  late QuizData randomQuizData;
  late int correctAnswerIndex;
  late List<String> answerOptions;
  List<Color> answerCardColors = [
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey
  ];

  void checkAnswer(String selectedAnswer) {
    setState(() {
      isAnswerCorrect = selectedAnswer == randomQuizData.title;

      if (isAnswerCorrect) {
        consecRightAnswers++;
        PlayerData.addExperience(
          1 + getMultiplierByConsec(consecRightAnswers),
        );
        // Change the color of the selected answer to green
        answerCardColors[selectedAnswerIndex] = Colors.green;
      } else {
        consecRightAnswers = 0;
        // Change the color of the selected answer to red
        answerCardColors[selectedAnswerIndex] = Colors.red;
        // Change the color of the correct answer to green
        answerCardColors[correctAnswerIndex] = Colors.green;
      }
      maxLevel = getNextMultiplierMilestone(consecRightAnswers) -
          getLastMultiplierMilestone(consecRightAnswers);
      currentLevel =
          consecRightAnswers - getLastMultiplierMilestone(consecRightAnswers);
      textScale = 1.0 + getMultiplierByConsec(consecRightAnswers) * 0.03;
      //print("Steps: $maxLevel");
      //print("curr.: $currentLevel");
    });

    // Delay for 1 second before loading the next quiz data
    Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        // Reset selectedAnswerIndex to -1
        selectedAnswerIndex = -1;
        // Reset answer card colors to default grey
        answerCardColors = [Colors.grey, Colors.grey, Colors.grey, Colors.grey];
      });
      _loadNextQuizData();
    });
  }

  @override
  void initState() {
    super.initState();
    _loadNextQuizData();
  }

  Future<void> _loadNextQuizData() async {
    final allQuizData = widget.database.values.toList();
    allQuizData.shuffle();
    setState(() {
      answerCardColors = [Colors.grey, Colors.grey, Colors.grey, Colors.grey];
      quizData.clear();
      quizData.addAll(allQuizData);
      if (quizData.isNotEmpty) {
        randomQuizData = quizData.first;
      }
      answerOptions =
          _generateAnswerOptions(); // Generate a list of answer options
      correctAnswerIndex = _findCorrectAnswerIndex(
          randomQuizData.title); // Set the correct answer index
    });
  }

  int _findCorrectAnswerIndex(String correctAnswer) {
    for (int i = 0; i < answerOptions.length; i++) {
      if (answerOptions[i].trim().toLowerCase() ==
          correctAnswer.trim().toLowerCase()) {
        return i;
      }
    }
    return -1; // Handle the case when correct answer is not found
  }

  List<String> _generateAnswerOptions() {
    final List<String> allTitles = quizData.map((data) => data.title).toList();
    allTitles.shuffle();

    // Ensure the correct answer is not in the options.
    List<String> answerOptions = allTitles.take(3).toList();
    while (answerOptions.contains(randomQuizData.title)) {
      allTitles.shuffle();
      answerOptions = allTitles.take(3).toList();
    }

    answerOptions.add(randomQuizData.title);
    answerOptions.shuffle();

    return answerOptions;
  }

  void _onAnswerButtonTap(String selectedAnswer) {
    setState(() {
      // Find the index of the selected answer in answerOptions
      selectedAnswerIndex = answerOptions.indexOf(selectedAnswer);
      //turn the color of the selected answer to yellow DBFF00
      //only one answer can be selected at a time
      answerCardColors[0] = selectedAnswerIndex == 0
          ? const Color.fromRGBO(219, 255, 0, 1)
          : Colors.grey;
      answerCardColors[1] = selectedAnswerIndex == 1
          ? const Color.fromRGBO(219, 255, 0, 1)
          : Colors.grey;
      answerCardColors[2] = selectedAnswerIndex == 2
          ? const Color.fromRGBO(219, 255, 0, 1)
          : Colors.grey;
      answerCardColors[3] = selectedAnswerIndex == 3
          ? const Color.fromRGBO(219, 255, 0, 1)
          : Colors.grey;
    });
    buttonActive = true;
  }

  /*Autor: Felix
  Gibt den aktuellen Multiplikator anhand
  von in Folge richtig erratenen Memes zurück.

  Da diese Funktion von allen anderen für die
  Berechnungen benutzt wird, können hier die
  Schwellwerte verändert werden, um anzupassen,
  wann welcher Multiplikator erreicht wird, oder 
  es können neue Multiplikatoren hinzugefügt 
  werden.
  */
  double getMultiplierByConsec(int consec) {
    switch (consec) {
      case 0:
        return 0;
      case 1:
        return 1;
      case >= 2 && < 6:
        return 2;
      case >= 6 && < 9:
        return 3;
      case >= 9 && < 16:
        return 5;
      case >= 16 && < 24:
        return 7;
      case >= 24:
        return 10;
    }
    return 0;
  }

  /*Autor: Felix
  Errechnet und gibt den nächsten Multiplikator
  zurück, der erreicht wird, wenn mehr Memes in Folge
  richtig erraten werden.
  */
  double getNextMultiplier(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    double nextMultiplier = currentMultiplier;
    for (int i = 0; i < 50; i++) {
      if (getMultiplierByConsec(currentConsec + i) > currentMultiplier) {
        nextMultiplier = getMultiplierByConsec(currentConsec + i);
        return nextMultiplier;
      }
    }
    return currentMultiplier;
  }

  /*Autor: Felix
  Errechnet und gibt den vorherigen Multiplikator anhand
  der Anzahl der aktuell richtig erratenen Memes in Folge
  zurück.
  */
  double getLastMultiplier(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    double lastMultiplier = currentMultiplier;
    for (int i = 0; i < 50; i++) {
      if (getMultiplierByConsec(currentConsec - i) < currentMultiplier) {
        lastMultiplier = getMultiplierByConsec(currentConsec - i);
        return lastMultiplier;
      }
    }
    return currentMultiplier;
  }

  /*Autor: Felix
  Errechnet und gibt die Menge der nötigen, in Folge
  richtig erratenen Memes zurück, die benötigt werden,
  um zum nächsten Multiplikator aufzusteigen, anhand
  der aktuell in Folge richtig erratenen Memes.
  */
  int getNextMultiplierMilestone(int currentConsec) {
    int nextMilestone = currentConsec;
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    for (int i = 0; i < 50; i++) {
      if (getMultiplierByConsec(currentConsec + i) > currentMultiplier) {
        nextMilestone = currentConsec + i;
        return nextMilestone;
      }
    }
    return 999999;
  }

  /*Autor: Felix
  Errechnet und gibt die Menge der in Folge richtig
  erratenen Memes zurück, die nötig gewesen sind, um
  den vorherigen Multiplikator zu erreichen, anhand der
  aktuell in Folge richtig erratenen Memes.
  */
  int getLastMultiplierMilestone(int currentConsec) {
    double currentMultiplier = getMultiplierByConsec(currentConsec);
    for (int i = 0; i < 50; i++) {
      if (getMultiplierByConsec(currentConsec - i) < currentMultiplier) {
        return currentConsec - i + 1;
      }
    }
    return 0;
  }

//disable Weiter Button
  bool buttonActive = false;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: Scaffold(
        body: Center(
            child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: LayoutBuilder(builder: (context, constraints) {
            return SingleChildScrollView(
                child: ConstrainedBox(
                    constraints:
                        BoxConstraints(minHeight: constraints.maxHeight),
                    child: IntrinsicHeight(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Spacer(flex: 1),
                        const SizedBox(height: 20),
                        Container(
                          //width: MediaQuery.of(context).size.width / 2.25,
                          //height: MediaQuery.of(context).size.height * 0.5,
                          height: 500,
                          width: 500,
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 7, 7, 7),
                            border: Border.all(
                                color: const Color.fromARGB(255, 221, 255, 0)),
                            borderRadius: const BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Column(
                            children: [
                              const Spacer(flex: 1),
                              const SizedBox(height: 20),
                              Expanded(
                                flex: 10,
                                child: SizedBox(
                                  height: 200,
                                  child: SizedBox(
                                    child: randomQuizData.image.isNotEmpty
                                        ? Image.asset(
                                            randomQuizData.image,
                                            fit: BoxFit.contain,
                                          )
                                        : Container(color: Colors.red),
                                  ),
                                ),
                              ),
                              const Spacer(flex: 1),
                              SizedBox(
                                height: 45,
                                width: 350,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "$consecRightAnswers",
                                      style: const TextStyle(
                                          color: Color.fromARGB(
                                              150, 255, 255, 255),
                                          fontSize: 20),
                                    ),
                                    Text(
                                      "${getNextMultiplierMilestone(consecRightAnswers)}",
                                      style: const TextStyle(
                                          color: Color.fromARGB(
                                              150, 255, 255, 255),
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 350,
                                child: LinearProgressBar(
                                  maxSteps: maxLevel,
                                  progressType:
                                      LinearProgressBar.progressTypeLinear,
                                  currentStep: currentLevel,
                                  progressColor:
                                      const Color.fromRGBO(96, 250, 251, 1),
                                  backgroundColor:
                                      const Color.fromRGBO(29, 28, 31, 1),
                                ),
                              ),
                              const Spacer(flex: 1),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "${getLastMultiplier(consecRightAnswers)}x",
                                    style: const TextStyle(
                                        color:
                                            Color.fromARGB(150, 255, 255, 255),
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Row(
                                    children: [
                                      const Icon(
                                        Icons.flash_on,
                                        size: 50,
                                        color: Color.fromRGBO(96, 250, 251, 1),
                                      ),
                                      AnimatedScale(
                                        scale: textScale,
                                        duration: const Duration(milliseconds: 300),
                                        curve: const MultiplierCurve(),
                                        child: Text(
                                          "${getMultiplierByConsec(consecRightAnswers)}x",
                                          style: const TextStyle(
                                            fontSize: 50,
                                            color: Color.fromARGB(
                                                255, 255, 255, 255),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "${getNextMultiplier(consecRightAnswers)}x",
                                    style: const TextStyle(
                                      color: Color.fromARGB(150, 255, 255, 255),
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Spacer(flex: 1),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Spacer(flex: 4),
                            answerCard(
                              answerOptions[0],
                              context,
                              () => _onAnswerButtonTap(answerOptions[0]),
                              answerCardColors[0],
                            ),
                            answerCard(
                              answerOptions[1],
                              context,
                              () => _onAnswerButtonTap(answerOptions[1]),
                              answerCardColors[1],
                            ),
                            const Spacer(flex: 4),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Spacer(flex: 4),
                            answerCard(
                              answerOptions[2],
                              context,
                              () => _onAnswerButtonTap(answerOptions[2]),
                              answerCardColors[2],
                            ),
                            answerCard(
                              answerOptions[3],
                              context,
                              () => _onAnswerButtonTap(answerOptions[3]),
                              answerCardColors[3],
                            ),
                            const Spacer(flex: 4),
                          ],
                        ),
                        const Spacer(flex: 1),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                            width: 200,
                            height: 50,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        const Color.fromARGB(255, 14, 18, 20))),
                                onPressed: buttonActive
                                    ? () {
                                        setState(() => buttonActive = false);
                                        checkAnswer(
                                            answerOptions[selectedAnswerIndex]);
                                      }
                                    : null,
                                child: Text("Weiter",
                                    style: buttonActive
                                        ? const TextStyle(color: Colors.white)
                                        : const TextStyle(
                                            color: Color.fromARGB(
                                                120, 255, 255, 255)))
                                //child: Text("Weiter"),
                                )),
                        const Spacer(flex: 5),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ))));
          }),
        )),
      ),
    );
  }
}

/*Autor: Felix
Eine stark verstärkte Standardkurve, um
die Zahl für die Animation stark zu vergrößern.  
*/
class MultiplierCurve extends Curve {
  const MultiplierCurve({
    this.multiplier = 20,
  });
  final double multiplier;

  @override
  double transformInternal(double t) {
    return Curves.easeInOutBack.transformInternal(t) * multiplier;
  }
}
