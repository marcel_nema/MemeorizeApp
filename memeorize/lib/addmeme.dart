/*
@Autorin: Charlotte

Hier wird die Seite zum Hinzufügen eines Memes erstellt.
Die Seite besteht aus einem Container mit einem imagePicker, zwei TextFormFields und zwei Buttons.
Mithilfe des imagePickers kann ein Bild aus der Galerie ausgewählt werden.
Die TextFormFields dienen zum Eingeben des Titels und der Beschreibung des Memes.
Über den Button "RESET" können die TextFormFields und der imagePicker zurückgesetzt werden.
Über den Button "SAVE" wird das Meme in der Datenbank gespeichert.

@Mitwirkender: Hendrik
Firebase Funktionalität optimiert

---------------------------------------------------------------------------------*/
import 'dart:html' as html;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:memeorize/firebaseservice.dart';

class AddMeme extends StatefulWidget {
  const AddMeme({super.key});

  @override
  AddMemeState createState() => AddMemeState();
}

class AddMemeState extends State<AddMeme> {
  FirebaseService firebaseService = FirebaseService();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _infoController = TextEditingController();
  final FocusNode _titleFocusNode = FocusNode();
  final FocusNode _infoFocusNode = FocusNode();
  Uint8List? _imageBytes;
  late XFile? pickedFile;

  final ImagePicker _imagePicker = ImagePicker();


    // _pickImage() öffnet den imagePicker
    // Wenn ein Bild ausgewählt wurde, wird es in _imageBytes gespeichert
    // Wenn kein Bild ausgewählt wurde, wird _imageBytes auf null gesetzt
    Future<void> _pickImage() async {
    try {
        pickedFile = await _imagePicker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 50,
      );

      if (pickedFile != null) {
        final Uint8List bytes = await pickedFile!.readAsBytes();
        // speichert das Bild in _imageBytes und zeigt es an
        setState(() {
          _imageBytes = bytes;
        });
      }
    } catch (e) {
      print('Error picking image: $e');
    }
  }




  // _saveMeme() speichert das Meme in der Datenbank
  void _saveMeme() async {
    if (_imageBytes == null) {
      _showPopup('Please upload an image before saving.');
    } else if (_titleController.text.isEmpty) {
      _showPopup('Title is required.');
    } else {
      try {

        // Upload des Bildes
        //final String? gsLink = await uploadFile(pickedFile, 'memes/${_titleController.text}.jpeg');


        // Meme in Firestore speichern
        await firebaseService.addMeme(
          _titleController.text,
          _infoController.text,
          html.File(_imageBytes!, 'meme.jpeg'),
          pickedFile,
        );
        // Kurzes Feedback anzeigen
        _showPopup('Meme saved successfully.');

        // Felder leeren
        _clearFields();
      } catch (e) {
        _showPopup('Error saving meme: $e');
      }
    }
  }



  // _showPopup() zeigt ein Popup mit einer Nachricht an
  // Genutzt für Fehlermeldungen und Feedback
  void _showPopup(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: const Color.fromARGB(255, 14, 18, 20),
          title: const Text('Info', style: TextStyle(color: Colors.white)),
          contentTextStyle: const TextStyle(color: Colors.white),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('OK', style: TextStyle(color: Colors.white)),
            ),
          ],
        );
      },
    );
  }


  // _clearFields() leert die TextFormFields und setzt das Bild zurück
  void _clearFields() {
    _titleController.clear();
    _infoController.clear();
    setState(() {
      _imageBytes = null;
    });
  }


  @override
  void dispose() {
    // Die FocusNodes und Controller müssen manuell entsorgt werden, 
    // da sie sonst nicht freigegeben werden
    _titleFocusNode.dispose();
    _infoFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: screenSize.height),
          child: Container(
            color: const Color.fromARGB(255, 14, 18, 20),
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //const SizedBox(height: 10),
                const AutoSizeText('CREATE MEME', style: TextStyle(fontSize: 40, color: Colors.white), textAlign: TextAlign.center),
                //const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: screenSize.width / 2.5,
                    height: screenSize.height / 3,
                    child: GestureDetector(
                      // Wenn auf den Container getippt wird, wird der imagePicker geöffnet
                      onTap: () async {
                        await _pickImage();
                      },
                      child: Container(
                        color: const Color.fromARGB(255, 0, 0, 1),
                        alignment: Alignment.center,
                        child: _imageBytes != null
                        // Wenn ein Bild ausgewählt wurde, wird dieses angezeigt
                        // Ansonsten wird ein Icon angezeigt, das zum Auswählen eines Bildes auffordert
                          ? Image.memory(_imageBytes!, fit: BoxFit.cover)
                          : Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.camera_alt,
                                    color: Colors.white,
                                    size: screenSize.height / 15,
                                  ),
                                  const SizedBox(height: 8),
                                  const AutoSizeText( // AutoSizeText passt den Text automatisch an die Größe des Containers an
                                    'UPLOAD IMAGE',
                                    style: TextStyle(fontSize: 16, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: screenSize.width / 2.5,
                  child: TextFormField(
                    focusNode: _titleFocusNode, 
                    // FocusNode wird benötigt, um zu erkennen, ob das Feld fokussiert ist
                    controller: _titleController,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      labelText: 'TITLE',
                      hintText: 'TITLE',
                      floatingLabelBehavior: FloatingLabelBehavior.auto, // Sorgt dafür, dass das Label oben bleibt, wenn Text eingegeben wird
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          // Wenn das Feld fokussiert ist, wird der Rand weiß, sonst transparent
                          color: _titleFocusNode.hasFocus ? Colors.white : Colors.transparent,
                        ),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      filled: true,
                      fillColor: const Color.fromARGB(255, 0, 0, 1),
                      hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                      labelStyle: const TextStyle(color: Colors.white),
                    ),
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: screenSize.width / 2.5,
                  child: TextFormField(
                    focusNode: _infoFocusNode,
                    controller: _infoController,
                    maxLength: 420,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      labelText: 'INFO',
                      hintText: 'INFO',
                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: _infoFocusNode.hasFocus ? Colors.white : Colors.transparent,
                        ),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      filled: true,
                      fillColor: const Color.fromARGB(255, 0, 0, 1),
                      hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                      labelStyle: const TextStyle(color: Colors.white),
                      counterStyle: const TextStyle(color: Colors.white),
                      counterText: '${_infoController.text.length}/420',
                    ),
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    minLines: 1,
                    maxLines: 5,
                    // Wenn sich der Text ändert, wird die Seite neu geladen, 
                    // damit der Counter aktualisiert wird
                    onChanged: (text) => setState(() {}),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: screenSize.width / 7,
                      height: screenSize.height / 20,
                      child: GestureDetector(
                        onTap: () {
                          // Felder leeren
                          _clearFields();
                        },
                        child: Container(
                          color: const Color.fromARGB(255, 92, 93, 97),
                          alignment: Alignment.center,
                          child: const AutoSizeText(
                            'RESET',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 20),
                    SizedBox(
                      width: screenSize.width / 7,
                      height: screenSize.height / 20,
                      child: GestureDetector(
                        onTap: () {
                          // Meme speichern
                          _saveMeme();
                        },
                        child: Container(
                          color: const Color.fromARGB(255, 219, 255, 0),
                          alignment: Alignment.center,
                          child: const AutoSizeText(
                            'SAVE',
                            style: TextStyle(fontSize: 14, color: Color(0xff0e1214)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }



}
