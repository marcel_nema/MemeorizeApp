import "package:cloud_firestore/cloud_firestore.dart";
import 'dart:math';

class QuizDataFirebase {
  static final QuizDataFirebase _instance = QuizDataFirebase._internal();
  Map<String, String>? documents;

  factory QuizDataFirebase() {
    return _instance;
  }

  QuizDataFirebase._internal() {
    initializeDocuments();
  }

  Future<void> initializeDocuments() async {
    try {
      documents = await _fetchAllDocuments("memes");
    } catch (e) {
      print("Fehler beim Initialisieren der Dokumente: $e");
      documents = {};
    }
  }

  Future<Map<String, String>?> _fetchAllDocuments(String collectionPath) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot =
        await firestore.collection(collectionPath).get();
    return {
      for (var doc in querySnapshot.docs)
        doc['title']: doc['imageUrl'] ?? 'Default.png'
    };
  }

  Future<Map<String, String>> randomeMemeTitleAndUrl() async {
    if (documents == null || documents!.isEmpty) {
      // Versuche, die Dokumente erneut zu laden oder einen Fehler zurückzugeben.
      await initializeDocuments();
      if (documents == null || documents!.isEmpty) {
        throw Exception('Keine Dokumente verfügbar.');
      }
    }
    var randomIndex = Random().nextInt(documents!.length);
    var mapEntry = documents!.entries.elementAt(randomIndex);
    return {"title": mapEntry.key, "imageUrl": mapEntry.value};
  }

  Future<Map<String, bool>> generateQuestionAndAnswers(
      String correctTitle) async {
    if (documents?.isEmpty ?? true) {
      await _fetchAllDocuments("memes");
    }

    Map<String, bool> questionAndAnswers = {};
    List<String> allTitles = documents?.keys.toList() ?? [];
    questionAndAnswers[correctTitle] = true;
    allTitles.remove(correctTitle);
    allTitles.shuffle(Random());
    for (var i = 0; i < 3; i++) {
      questionAndAnswers[allTitles[i]] = false;
    }
    return questionAndAnswers;
  }
}
