import 'package:flutter/material.dart';

/*--------------------------------------------------------------------------

Karli's Bereich und angepasst von Marcel

Widget zum erstellen einer Auswahlkarte des Meme-Trainers

*/ //-----------------------------------------------------------------------

Widget answerCard(
    String text, BuildContext context, Function onTap, Color cardColor) {
  return Expanded(
      flex: 10,
      child: SizedBox(
        height: 80,
        child: GestureDetector(
          onTap: onTap as void Function()?,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3),
            ),
            elevation: 2,
            color: cardColor, // Background Color is set here
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ));
}
