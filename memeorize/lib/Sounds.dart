
import 'dart:math';

import 'package:flutter/foundation.dart';


class SoundEffects{

  // Methode returned ein zufällig korrekt-Sound
  String randomFailSound() {
    List<String> FailsSounds = [
      "assets/sounds/wrong/wrong-buzzer.mp3",
      "assets/sounds/wrong/negative_beeps.mp3",
      "assets/sounds/wrong/failure-drum.mp3",
      "assets/sounds/wrong/buzzer-4.mp3",
      "assets/sounds/wrong/sus-meme-sound.mp3",
      "assets/sounds/wrong/bruh.mp3",
      "assets/sounds/wrong/windows_error.mp3",
      "assets/sounds/wrong/steve-old-hurt.mp3",
      "assets/sounds/wrong/vine-boom.mp3",
      "assets/sounds/wrong/das-gibt-ne-anzeige-von-karin-ritter.mp3",
      "assets/sounds/wrong/gta-san-andreas-ah-shit-here-we-go-again.mp3",
      "assets/sounds/wrong/huh_37bAoRo.mp3",
      "assets/sounds/wrong/klonk.mp3",
      "assets/sounds/wrong/loading-lost-connection.mp3",
      "assets/sounds/wrong/ny-video.mp3",
      "assets/sounds/wrong/punchmp3.mp3",
      "assets/sounds/correct/you-win.mp3",
      "assets/sounds/wrong/hello-darkness.mp3",
      "assets/sounds/wrong/hunger-games-whistle.mp3",
      "assets/sounds/wrong/i-dont-think-so.mp3",
      "assets/sounds/wrong/not-a-good-start-boris.mp3",
      "assets/sounds/wrong/wrong-trump.mp3",



      


      

      ];
    if(kIsWeb) {
      return "assets/" + FailsSounds[Random().nextInt(FailsSounds.length)];
    }
    return FailsSounds[Random().nextInt(FailsSounds.length)];
  }

  // Methode returned ein zufällig korrekt-Sound
    String randomCorrectSound() {
    List<String> CorrectSounds = [
      "assets/sounds/correct/yeah-boy.mp3",
      "assets/sounds/correct/training-program.mp3",
      "assets/sounds/correct/collect.mp3",
      "assets/sounds/correct/correct-voice.mp3",
      "assets/sounds/correct/got-you.mp3",
      "assets/sounds/correct/stonks.mp3",
      "assets/sounds/correct/anime-wow.mp3",
      "assets/sounds/correct/alright_alright_alright.mp3",
      "assets/sounds/correct/big-league-trump.mp3",
      "assets/sounds/correct/bravo.mp3",
      "assets/sounds/correct/congratulation.mp3",
      "assets/sounds/correct/easy.mp3",
      "assets/sounds/correct/halleluja.mp3",
      "assets/sounds/correct/here-we-go-mario.mp3",
      "assets/sounds/correct/i-feel-good.mp3",
      "assets/sounds/correct/kids-cheering-yay.mp3",
      "assets/sounds/correct/level-up.mp3",
      "assets/sounds/correct/nice.mp3",
      "assets/sounds/correct/noice.mp3",
      "assets/sounds/correct/oh-yes-undertale.mp3",
      "assets/sounds/correct/okay-lets-go.mp3",
      "assets/sounds/correct/sheesh.mp3",
      "assets/sounds/correct/skrrt-skrrt.mp3",
      "assets/sounds/correct/we-got-a-badass.mp3",
      "assets/sounds/correct/woohoo-homer-simpson.mp3",






      

      ];
    if(kIsWeb) {
      return "assets/" + CorrectSounds[Random().nextInt(CorrectSounds.length)];
    }
    return CorrectSounds[Random().nextInt(CorrectSounds.length)];
  }




}