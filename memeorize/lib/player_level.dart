/*

Autor dieser Datei:
Felix Langrehr

Diese Datei dient Zur Anzeige des
aktuellen Spielerlevels in der Leiste
links am Bildschirmrand und auf der
Profilseite. Die Erfahrungspunkte zum
nächsten Level werden als ProgressBar
dargestellt (ProgressBar von Karli kopiert).

*/

import 'package:flutter/material.dart';
import 'package:linear_progress_bar/linear_progress_bar.dart';
import 'package:memeorize/admin_password_field.dart';
import 'package:memeorize/firebaseservice.dart';
import 'package:memeorize/ranking_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlayerLevel extends StatefulWidget {
  static SharedPreferences? prefs;
  static final FirebaseService _firebaseService = FirebaseService();
  double expWidth = 200.0;
  PlayerLevel({
    super.key,
    required this.expWidth,
  });
  @override
  State<PlayerLevel> createState() => _PlayerLevelState();
}

class _PlayerLevelState extends State<PlayerLevel> {
  static List<_PlayerLevelState> pls = [];

  @override
  void initState() {
    pls.add(this);
    super.initState();
    for (int i = 0; i < RankingBarState.instances.length; i++) {
      Future.delayed(
        const Duration(milliseconds: 200),
        () {
          RankingBarState.instances[i]
              .refreshPlayerInfo(PlayerData.level, callSetState: true);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    //PlayerData.loadPlayerData();
    int showPlayerLevel = PlayerData.level;
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "LVL: $showPlayerLevel",
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 26,
                ),
              ),
            ],
          ),
          Container(
            height: 10,
          ),
          SizedBox(
            width: widget.expWidth,
            child: LinearProgressBar(
              maxSteps: 100,
              progressType: LinearProgressBar.progressTypeLinear,
              currentStep: (PlayerData.experience /
                      PlayerData._experienceNeededPerLevel *
                      100)
                  .toInt(),
              progressColor: const Color.fromRGBO(96, 250, 251, 1),
              backgroundColor: const Color.fromRGBO(29, 28, 31, 1),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    pls.remove(this);
    super.dispose();
  }

  void rebuild() {
    setState(() {});
  }
}

class PlayerData {
  static int level = 1;
  static int experience = 0;
  static const int _experienceNeededPerLevel = 50;
  static String playerName = "";
  static String userID = "";
  static final FirebaseService _firebaseService = FirebaseService();

  static void addExperience(double addedExperience) {
    experience += addedExperience.toInt();
    level += (experience / _experienceNeededPerLevel).floor();
    experience = experience % _experienceNeededPerLevel;
    for (int i = 0; i < _PlayerLevelState.pls.length; i++) {
      _PlayerLevelState.pls[i].rebuild();
    }
    for (int i = 0; i < RankingBarState.instances.length; i++) {
      RankingBarState.instances[i].refreshPlayerInfo(level);
    }
    savePlayerData();
  }

  static void savePlayerData() {
    try {
      final SharedPreferences prefs = PlayerLevel.prefs!;
      prefs.setInt("level", level);
      prefs.setInt("exp", experience);
      prefs.setString("name", playerName);
      if (userID == "") {
        final now = DateTime.now();
        userID =
            "${now.year}-${now.month}-${now.day}-${now.hour}-${now.minute}-${now.second}-${now.millisecond}-${now.microsecond}";
      }
      prefs.setString("uid", userID);
      //print(
      //"Efolgreich Spielerdaten gespeichert: $level, $experience, $userID");
      String pName = userID;
      if (playerName != "" && playerName != "YourName") {
        pName = playerName;
      }
      //_firebaseService.updatePlayer(userID, level, pName, experience);
      _firebaseService.updatePlayerSupabase(userID, level, pName, experience);
    } on Exception {
      print("Fehler beim Speichern der Spielerdaten.");
    }
  }

  static Future<void> loadPlayerDataFromOnline() async {
    final SharedPreferences prefs = PlayerLevel.prefs!;
    if (prefs.getString("uid") != null) {
      userID = prefs.getString("uid")!;
      //print("User: $userID");
    }
    //RetrievePlayerData? retData = await _firebaseService.getSpecificPlayer(
    //    userID, UserDataEncoder.encodeUID(userID));

    RetrievePlayerData? retData = await _firebaseService
        .getSpecificPlayersSupabase(userID, UserDataEncoder.encodeUID(userID));
    if (retData == null || retData.userID == "") {
      oldLoadPlayerData();
    } else {
      level = retData.level;
      playerName = retData.playerName;
      experience = retData.experience;
    }
  }

  static void oldLoadPlayerData() {
    try {
      final SharedPreferences prefs = PlayerLevel.prefs!;
      if (prefs.getInt("level") != null && prefs.getInt("exp") != null) {
        level = prefs.getInt("level")!;
        experience = prefs.getInt("exp")!;
        //print("Efolgreich Spielerdaten geladen: $level, $experience");
      }
      if (prefs.getString("uid") != null) {
        userID = prefs.getString("uid")!;
        //print("User: $userID");
      }
      if (prefs.getString("name") != null) {
        playerName = prefs.getString("name")!;
        //print("Spielername: $playerName");
      }
    } on Exception {
      print(
          "Fehler beim Laden der Spielerdaten. Eventuell wurden noch keine Spielerdaten gespeichert.");
    }
  }
}

class RetrievePlayerData {
  int level = 1;
  int experience = 0;
  String playerName = "";
  String userID = "";

  RetrievePlayerData(int _level, String _playerName, String _userID) {
    level = _level;
    playerName = _playerName;
    userID = _userID;
  }
}
