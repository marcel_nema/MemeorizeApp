import 'package:flutter/material.dart';

const Color MemeGreen = Color(0xFFDBFF00);
const Color MemeBlue = Color.fromRGBO(96, 250, 251, 1);
const Color MemeGrey = Color.fromRGBO(92, 93, 97, 1);
const Color MemeDarkGrey = Color(0xFF1D1C1F);
const Color MemeRed = Color.fromRGBO(255, 0, 76, 1);
const Color MemeBlackGreen = Color.fromARGB(255, 14, 18, 20);
Gradient ComicSansGradient = const LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [
    Color(0xFF00FF00),
    Color(0xFF00FFFF),
    Color(0xFF0000FF),
    Color(0xFFFF00FF),
    Color(0xFFFF0000),
    Color(0xFFFFFF00),
    Color(0xFF00FF00),
  ],
);


// FONT

const TextStyle MemeFont = TextStyle(
  fontFamily: 'BebasNeue',
);

const TextStyle MemeFontPermanentMarker = TextStyle(
  fontFamily: 'Permanent_Marker',
);
