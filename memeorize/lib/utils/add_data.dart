

import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';


/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hierüber werden die Daten die in EditProfilePage geändert werden in Firebase geladen 
und gespeichert.
*/ //     --------------------------------//\\--------------------------------

final FirebaseStorage _storage = FirebaseStorage.instance;
final FirebaseFirestore _firestore = FirebaseFirestore.instance;

class StoreData{

  Future<String> uploadImageToStorage(String childName, Uint8List file) async {
    Reference ref = _storage.ref().child(childName);
    UploadTask uploadTask = ref.putData(file);
    TaskSnapshot snapshot = await uploadTask;
    String downloadURL = await snapshot.ref.getDownloadURL();
    return downloadURL;
  }

  Future<String> saveData({required String name, required String email, required Uint8List file}) async {
    String resp = "Some error occurred";
    try{
      if(name.isNotEmpty || email.isNotEmpty) {
        String imageURL = await uploadImageToStorage('profileImage', file);
        await _firestore.collection('userProfile').add({
          'name' : name,
          'email' : email,
          'imageLink' : imageURL
        });
      resp = 'success';
      }
    }

     
    catch(err) {
      resp = err.toString();
    }
    return resp;
  }
}