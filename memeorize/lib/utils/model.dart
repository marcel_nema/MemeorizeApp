import 'package:flutter/foundation.dart';
import 'package:memeorize/player_level.dart';

/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hier wird die UserProfile clas gebaut, die über den Provider auf Änderungen in 
editProfilepage wartet und die geänderten Daten updatet.
*/ //     --------------------------------//\\--------------------------------

class UserProfile {
  final String name;
  final String email;
  final Uint8List? image;

  UserProfile({required this.name, required this.email, this.image});
}

class UserProfileProvider with ChangeNotifier {
  late UserProfile _userProfile =
      UserProfile(name: checkInitialName(), email: 'memeorize@email.de');

  UserProfile get userProfile => _userProfile;

  void updateUserProfile(UserProfile newUserProfile) {
    _userProfile = newUserProfile;
    notifyListeners();
  }

  String checkInitialName() {
    if (PlayerData.playerName != "" &&
        PlayerData.playerName != "YourName") {
      return PlayerData.playerName;
    }
    return 'YourName';
  }
}
