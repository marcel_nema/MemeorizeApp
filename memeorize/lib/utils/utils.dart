import 'package:image_picker/image_picker.dart';


/*     --------------------------------//\\--------------------------------
Diese Datei wurde von Nele erstellt.
Hier wird der ImagePicker initialisiert um Fotos aus der Gallerie als Profil-Foto 
zu wählen. (siehe edit_profile.dart)
*/ //     --------------------------------//\\--------------------------------

pickImage(ImageSource source) async {
  final ImagePicker imagePicker = ImagePicker();//pick images from devices gallery
  XFile? file = await imagePicker.pickImage(source: source);
  //check if file has an image
  if( file != null) {
    return await file.readAsBytes();
  }
  print('No images selected');
}