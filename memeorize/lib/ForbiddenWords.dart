/*
  Datei dient zur Überprüfung von Eingaben
  (Namen, die bei anderen angezeigt werden)
  auf Wörter, die nicht enthalten sein dürfen
  Sodass sich Spieler nicht bspw. "H**ler" o.Ä.
  nennen können und so in der Rangliste auftauchen.
*/
import 'dart:convert';

class ForbiddenWords {
  static List<List<int>> forbiddenWordsList = [
    [104, 105, 116, 108, 101, 114],
    [110, 105, 103, 103, 101, 114],
    [110, 101, 103, 101, 114],
    [107, 121, 115],
    [104, 117, 115, 111],
    [104, 117, 114, 101, 110, 115, 111, 104, 110],
    [99, 117, 110, 116],
    [102, 111, 116, 122, 101],
    [97, 114, 115, 99, 104],
    [112, 105, 115, 115, 101, 114],
    [109, 111, 116, 104, 101, 114, 102, 117, 99, 107, 101, 114],
    [115, 99, 104, 108, 97, 109, 112, 101],
    [98, 105, 116, 99, 104],
    [104, 117, 114, 101],
    [119, 104, 111, 114, 101],
    [115, 108, 97, 118, 101],
    [115, 107, 108, 97, 118, 101],
    [114, 97, 112, 101],
    [114, 97, 112, 105, 115, 116],
    [118, 101, 114, 103, 101, 119, 97, 108, 116, 105, 103],
    [107, 105, 108, 108, 97, 108, 108],
    [102, 195, 188, 104, 114, 101, 114],
    [114, 97, 115, 115, 101],
    [102, 105, 99, 107],
    [102, 117, 99, 107],
    [97, 99, 97, 98],
  ];

  static bool isNameOkay(String name) {
    bool isOkay = true;
    List<String> forbiddenWordsStrings = [];
    for (List<int> encoded in forbiddenWordsList) {
      forbiddenWordsStrings.add(utf8.decode(encoded));
    }
    for (String toChekOn in forbiddenWordsStrings) {
      String small = name.toLowerCase();
      String removedSpaces = small.trim();
      String removedDots = removedSpaces
          .replaceAll(".", "")
          .replaceAll(",", "")
          .replaceAll("-", "")
          .replaceAll("/", "")
          .replaceAll("#", "")
          .replaceAll(" ", "");
      if (removedDots.contains(toChekOn)) {
        isOkay = false;
      }
    }
    return isOkay;
  }
}
