/*
@Autoren: Felix, Hendrik, Charlotte

Diese Datei dient zur Kommunikation mit der Firebase-Datenbank.

----------------------------------------------------------------------*/

import 'dart:async';
import 'dart:html' as html;
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memeorize/admin_password_field.dart';
import 'package:memeorize/player_level.dart';
import 'package:memeorize/ranking_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class FirebaseService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _storage = FirebaseStorage.instance;

  /// Hochladen eines Bildes und Rückgabe des GS-Pfads
  Future<String> uploadImage(dynamic image) async {
    print('uploadImage wird aufgerufen...');
    if (identical(0, 0.0)) {
      // Plattform ist Web
      String fileName = 'memes/${DateTime.now().millisecondsSinceEpoch}.jpeg';
      Reference storageRef = _storage.ref().child(fileName);

      try {
        Uint8List data = await _readDataFromHtmlFile(
            image); // Konvertieren der Datei in Uint8List
        UploadTask uploadTask = storageRef.putData(data);
        final TaskSnapshot snapshot = await uploadTask.whenComplete(() {});
        final String downloadUrl = await snapshot.ref.getDownloadURL();
        print('Bild wurde erfolgreich hochgeladen: $fileName');
        print('Upload complete. File URL: $downloadUrl');
        return downloadUrl; //storageRef.fullPath;
      } catch (e) {
        print('Fehler beim Hochladen des Bildes: $e');
        return '';
      }
    } else {
      // Plattform ist Nicht-Web (zukünftige Erweiterung)
      print('Plattformspezifischer Code für Nicht-Web-Plattformen fehlt');
      return '';
    }
  }

  /// Lesen von Daten aus einer HTML-Datei
  Future<Uint8List> _readDataFromHtmlFile(dynamic file) {
    final completer = Completer<Uint8List>();
    final reader = html.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onLoadEnd.listen((event) {
      if (reader.result != null) {
        completer.complete(reader.result as Uint8List);
      } else {
        completer.completeError('Fehler beim Lesen der Datei');
      }
    });
    return completer.future;
  }

  /// Lädt die Datei hoch und gibt den Download-Link zurück.
  Future<String?> uploadFile(XFile? file, String folderURL) async {
    if (file == null) {
      return null;
    }

    try {
      final blob = html.Blob([await file.readAsBytes()]);
      final storageRef = FirebaseStorage.instance.ref().child(folderURL);
      SettableMetadata metadata = SettableMetadata(contentType: 'image/jpeg');

      final uploadTask = storageRef.putBlob(blob, metadata);
      await uploadTask.whenComplete(() {});

      // Erhalte den Download-URL.
      final downloadURL = await storageRef.getDownloadURL();
      print('Download-Link: $downloadURL');

      // Erhalte den gs:// Link.
      final gsLink = await storageRef.getDownloadURL();
      print('GS-Link: $gsLink');

      return gsLink; // Oder return downloadURL, abhängig davon, was du benötigst.
    } catch (e) {
      print('Error uploading file: $e');
      return null;
    }
  }

  /// Hinzufügen eines Memes in Firestore
  Future<void> addMeme(
      String title, String info, html.File image, XFile? file) async {
    print('addMeme wird aufgerufen...');
    String gsPath = await uploadImage(image);

    if (gsPath.isNotEmpty) {
      //String imageUrl = await getImageUrl(gsPath);

      final String? gsLink = await uploadFile(
          file, 'gs://memeorize-92e80.appspot.com/memes/$title.jpeg');

      try {
        await _firestore.collection('memes').add({
          'title': title,
          'info': info,
          'imageUrl': gsLink,
        });
        print('Meme wurde erfolgreich in Firestore gespeichert');
      } catch (e) {
        print('Fehler beim Speichern des Memes in Firestore: $e');
      }
    } else {
      print('GS-Pfad ist leer. Bild wurde möglicherweise nicht hochgeladen.');
    }
  }

  /// Abrufen der Download-URL aus dem GS-Pfad
  Future<String> getImageUrl(String? gsPath) async {
    print('getImageUrl wird aufgerufen...');
    if (gsPath != null) {
      try {
        String imageUrl = await _storage.ref(gsPath).getDownloadURL();
        print('Download-URL erfolgreich abgerufen: $imageUrl');
        return imageUrl;
      } catch (e) {
        print('Fehler beim Abrufen der Download-URL: $e');
        return '';
      }
    } else {
      print('GS-Pfad ist null');
      return '';
    }
  }

  /// Löschen eines Memes aus Firestore
  Future<void> deleteMeme(String id) async {
    await _firestore.collection('memes').doc(id).delete();
  }

  Future<void> updateMeme(
      String id, String title, String info, html.File image) async {
    String imageUrl = await uploadImage(image);
    await _firestore.collection('memes').doc(id).update({
      'title': title,
      'info': info,
      'imageUrl': imageUrl,
    });
  }

  /// Abrufen aller Memes aus Firestore
  Stream<QuerySnapshot<Map<String, dynamic>>> getMemesAsStream() {
    return _firestore.collection('memes').snapshots();
  }

  // Abrufen eines einzelnen Memes aus Firestore, nützlich für edit memes
  Future<void> updateMemeImage(String id, String imageUrl) async {
    await FirebaseFirestore.instance.collection('memes').doc(id).update({
      'imageUrl': imageUrl,
    });
  }

  /// Updated ein Meme ohne das Bild
  Future<void> updateMemeWithoutImage(
      String id, String title, String info) async {
    await _firestore.collection('memes').doc(id).update({
      'title': title,
      'info': info,
    });
  }

  Future<void> updatePlayer(String uid, int level, String name,
      [int experience = 0]) async {
    try {
      await _firestore.collection('players').doc(uid).set({
        'UID': uid,
        'Level': level,
        'Name': name,
        'Experience': experience,
      });
      print('Spielerdaten aktualisiert');
    } catch (e) {
      print('Fehler beim Speichern des Spielers in Firestore: $e');
    }
  }

  /// Gibt die Spielerdaten des aktuellen Spielers zurück
  /// Liste bestehend aus
  Future<List<Player>> getOtherPlayers() async {
    try {
      //var snapshot = await _firestore.collection('players').get();
      QuerySnapshot querySnapshot =
          await _firestore.collectionGroup('players').get();
      List<Player> resultPlayers = [];
      for (var queryDocumentSnapshot in querySnapshot.docs) {
        Map<String, dynamic> data =
            queryDocumentSnapshot.data() as Map<String, dynamic>;
        var level = data['Level'];
        var name = data['UID'];
        if (data['Name'] != null) {
          name = data['Name'];
        } else {
          name = data['UID'];
        }
        var uid = data['UID'];
        Player toAdd = Player(name, level);
        toAdd.userID = uid;
        resultPlayers.add(toAdd);
      }
      print('Spielerdaten aktualisiert');
      return resultPlayers;
    } catch (e) {
      print('Fehler beim Speichern des Spielers in Firestore: $e');
    }
    return [];
  }

  //Zum Wiedereinloggen auf einem anderen Gerät/Browser
  Future<RetrievePlayerData?> getSpecificPlayer(
      String searchForUID, String passcode) async {
    if (searchForUID == null || searchForUID == "") {
      return null;
    }
    if (passcode != UserDataEncoder.encodeUID(searchForUID)) {
      return null;
    }
    try {
      //var snapshot = await _firestore.collection('players').get();
      QuerySnapshot querySnapshot =
          await _firestore.collectionGroup('players').get();
      RetrievePlayerData? resultPlayer = null;
      for (var queryDocumentSnapshot in querySnapshot.docs) {
        Map<String, dynamic> data =
            queryDocumentSnapshot.data() as Map<String, dynamic>;
        if (data['UID'] == searchForUID) {
          var uid = data['UID'];
          var name = data['Name'];
          var level = data['Level'];
          var exp = data['Experience'];
          if (uid != null && name != null && level != null) {
            resultPlayer = RetrievePlayerData(level, name, uid);
          }
          if (resultPlayer != null && exp != null) {
            resultPlayer.experience = exp;
          }
        }
      }
      return resultPlayer;
    } catch (e) {
      print('Fehler beim Laden einer anderen UID: $e');
    }
    return null;
  }

  Future<List<Player>> getOtherPlayersSupabase() async {
    try {
      //var snapshot = await _firestore.collection('players').get();
      List<Player> resultPlayers = [];

      final _future =
          await Supabase.instance.client.from('playersData').select();
      //List<Map<String, dynamic>> data =
      //print("${_future.length}");
      for (var data in _future) {
        var level = data['Level'];
        var name = data['UserID'];
        if (data['Name'] != null) {
          name = data['Name'];
        } else {
          name = data['UserID'];
        }
        var uid = data['UserID'];
        Player toAdd = Player(name, level);
        toAdd.userID = uid;
        resultPlayers.add(toAdd);
      }
      print('Spielerdaten aktualisiert');
      return resultPlayers;
    } catch (e) {
      print('Supabase Error beim holen aller Spielerdaten: $e');
    }
    return [];
  }

  Future<RetrievePlayerData?> getSpecificPlayersSupabase(
      String searchForUID, String passcode) async {
    if (searchForUID == null || searchForUID == "") {
      return null;
    }
    if (passcode != UserDataEncoder.encodeUID(searchForUID)) {
      return null;
    }
    try {
      //var snapshot = await _firestore.collection('players').get();
      RetrievePlayerData? resultPlayer = null;

      final _future =
          await Supabase.instance.client.from('playersData').select();
      //List<Map<String, dynamic>> data =
      //print("${_future.length}");
      for (var data in _future) {
        if (data['UserID'] == searchForUID) {
          var uid = data['UserID'];
          var level = data['Level'];
          var name = data['Name'];
          var exp = data['Experience'];

          if (uid != null && name != null && level != null) {
            resultPlayer = RetrievePlayerData(level, name, uid);
          }
          if (resultPlayer != null && exp != null) {
            resultPlayer.experience = exp;
          }
        }
      }
      //print('Spielerdaten aktualisiert');
      return resultPlayer;
    } catch (e) {
      print('Supabase Error beim holen aller Spielerdaten: $e');
    }
    return null;
  }

  Future<void> updatePlayerSupabase(String uid, int level, String name,
      [int experience = 0]) async {
    try {
      final _future =
          await Supabase.instance.client.from('playersData').upsert({
        'UserID': uid,
        'Level': level,
        'Name': name,
        'Experience': experience,
      });
      print('Spielerdaten aktualisiert');
    } catch (e) {
      print('Fehler beim Speichern des Spielers in Firestore: $e');
    }
  }
}
