const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

// Konfigurieren Sie Nodemailer mit Ihrem E-Mail-Dienst
const transporter = nodemailer.createTransport({
  service: 'gmail', // Beispiel: Gmail
  auth: {
    user: 'IhreEmail@gmail.com',
    pass: 'IhrPasswort' // Verwenden Sie für die Produktion besser Umgebungsvariablen oder Firebase Config!
  }
});

exports.sendReportEmail = functions.firestore
    .document('reportedMemes/{memeId}')
    .onCreate((snap, context) => {
      const newReport = snap.data();
      const mailOptions = {
        from: 'IhreEmail@gmail.com',
        to: 'EmpfaengerEmail@example.com',
        subject: `Meme Report: ${newReport.memeTitle}`,
        text: `Ein Meme wurde gemeldet: ${newReport.memeId}`
      };

      return transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
      });
    });
